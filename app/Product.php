<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'slug', 'description', 'price', 'stock', 'destacated', 'subcategorie_id', 'season_id', 'image'];
 
    /**
     * getRouteKeyName
     * Retorna el slug a la url
     * @author David Navarrete
     * @return string slug
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    /**
     * images
     * Relacion de uno a muchos entre producto y productos-imagenes
     * @author David Navarrete
     * @return void
     */
    public function images()
    {
        return $this->hasMany('App\ProductImages');
    }
    
    /**
     * subcategorie
     * Relación de uno a muchos entre producto y subcategoria
     * @author David Navarrete
     * @return void
     */
    public function subcategorie()
    {
        return $this->belongsTo('App\Subcategorie');
    }
    
    /**
     * season
     * Relación de uno a muchos entre temporada y productos
     * @author David Navarrete
     * @return void
     */
    public function season()
    {
        return $this->belongsTo('App\Season');
    }
        
    /**
     * buys
     * Relación de muchos a muchos entre productos y compras hechas por el usuario
     * @author David Navarrete
     * @return void
     */
    public function buys(){
        return $this->belongsToMany(Buy::class, 'buy_user', 'buy_id', 'product_id')->withTimestamps();
    }
}
