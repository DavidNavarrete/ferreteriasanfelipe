<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    public $timestamps = false;
        
    /**
     * social
     * Relacion de uno a uno entre Icono y Social
     * @author David Navarrete
     * @return void
     */
    public function social()
    {
        return $this->hasOne('App\Social');
    }
}
