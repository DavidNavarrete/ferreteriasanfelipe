<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyCart extends Model
{
    protected $fillable = ['product_id', 'user_id', 'quantity'];
}
