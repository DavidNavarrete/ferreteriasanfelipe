<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
   /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'normal_working_hours_start' => 'required',
            'normal_working_hours_end' => 'required',
            'working_hours_sunday_start' => 'required',
            'working_hours_sunday_end' => 'required',
            'holidays_working_hours_start' => 'required',
            'holidays_working_hours_end' => 'required',
            'mission' => 'required|string',
            'vision' => 'required|string',
            'description' => 'required|string',
            'ubication' => 'required|string'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'normal_working_hours_start.required' => 'Ingrese el comienzo del horario normal',
            'normal_working_hours_end.required' => 'Ingrese el fin del horario normal',
            'working_hours_sunday_start.required' => 'Ingrese el comienzo del horario de Domingo',
            'working_hours_sunday_end.required' => 'Ingrese el fin del horario de Domingo',
            'holidays_working_hours_start.required' => 'Ingrese el comienzo del horario en días festivos',
            'holidays_working_hours_end.required' => 'Ingrese el fin del horario en días festivos',
            'mission.required' => 'Ingrese la misión de la empresa',
            'vision.required' => 'Ingrese la visión de la empresa',
            'description.required' => 'Ingrese la descripción de la empresa',
            'ubication.required' => 'Ingrese la ubicación de la empresa'
        ];
    }
}
