<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSeasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:seasons,name', 
            'image' => 'required|image|max:2048',
            'enabled' => 'required'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'image.required' => 'Seleccione la imagen',
            'image.max' => 'La imagen no debe superar los 2 MB',
            'image.image' => 'El archivo debe ser una imagen',
            'name.unique' => 'La temporada ya existe',
            'enabled.required' => 'Seleccione si desea habilitar la temporada'
        ];
    }
}
