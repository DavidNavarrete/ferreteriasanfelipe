<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'name' =>'required',
            'description' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'destacated' => 'required',
            'subcategorie_id' => 'integer|required|not_in:0',
            'season_id' => 'nullable',
            'image' => 'image|max:2048'
            ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'destacated.required' => 'El campo destacado es obligatorio',
            'subcategorie_id.required' => 'Seleccione una subcategoría',
            'image.max' => 'La imagen no debe superar los 2 MB',
            'image.image' => 'El archivo debe ser una imagen'
        ];
    }
}
