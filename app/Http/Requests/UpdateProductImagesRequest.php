<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer|not_in:0',
            'image1' => 'image|max:2048',
            'image2' => 'image|max:2048',
            'image3' => 'image|max:2048'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
        'image1.image' => 'El archivo debe ser una imagen',
        'image2.image' => 'El archivo debe ser una imagen',
        'image3.image' => 'El archivo debe ser una imagen',
        'image1.max' => 'La imagen no debe superar los 2 MB',
        'image2.max' => 'La imagen 2 no debe superar los 2 MB',
        'image3.max' => 'La imagen 3 no debe superar los 2 MB',
        'product_id.required' => 'Seleccione el producto que tendrá las imágenes'
        ];

    }
}
