<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubcategorieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'name' =>'required',
            'categorie_id' => 'integer|required|not_in:0'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'categorie_id.required' => 'Selecciona una categoría'
        ];
    }
}
