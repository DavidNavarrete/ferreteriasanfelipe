<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'name' =>'required|unique:products,name',
            'description' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'destacated' => 'required',
            'image' => 'required|image|max:2048',
            'subcategorie_id' => 'integer|required|not_in:0',
            'season_id' => 'nullable'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'image.required' => 'Selecciona la imagen del producto',
            'destacated.required' => 'El campo destacado es obligatorio',
            'subcategorie_id.required' => 'Seleccione una subcategoría',
            'image.max' => 'La imagen no debe superar los 2 MB',
            'image.image' => 'El archivo debe ser una imagen',
            'name.unique' => 'El producto ya existe'
        ];
    }
}
