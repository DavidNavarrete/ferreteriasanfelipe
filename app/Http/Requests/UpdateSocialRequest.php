<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   /**
     * rules
     * Valida los campos del formulario
     * @author David Navarrete
     * @return array Validaciones
     */
    public function rules()
    {
        return [
            'icon_id' => 'required',
            'name_profile' => 'required'
        ];
    }
    /**
     * messages
     * Muestra los mensajes de validación según corresponda
     * @author David Navarrete
     * @return array mensajes
     */
    public function messages()
    {
        return [
            'icon_id.required' => 'Selecciona una red social',
            'name_profile.required' => 'Ingrese el nombre del perfil de su página o número teléfonico'
        ];
    }
}
