<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use Illuminate\Support\Facades\Auth;


class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role_admin = Role::where('name', 'Admin')->pluck('id')->first();

        if(Auth::User()->role_id == $role_admin){
            return $next($request);
        }else{
            abort(401);
        }
        
    }
}
