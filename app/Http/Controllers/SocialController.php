<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSocialRequest;
use App\Http\Requests\UpdateSocialRequest;
use App\Social;
use App\Icon;
use Illuminate\Support\Str;

class SocialController extends Controller
{
    /**
     * index
     * Muestra la ventana principal de las redes sociales
     * @param  mixed $request HTTP Request buscador
     * @author David Navarrete
     * @return void
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('searchsocialnetwork'));
            $socialnetworks = Social::with('icon')->whereHas('icon', function($query2) use ($request){
                $query2->where('name', 'LIKE', '%'.trim($request->get('searchsocialnetwork')).'%');
            })->orWhere('name_profile', 'LIKE', '%'.$query.'%')->orderBy('name_profile')->paginate(8);
            return view('crudRedesSociales.listaRedesSociales', compact('socialnetworks', 'query'));
        }else{
            $socialnetworks = Social::with('icon')->orderBy('name_profile')->paginate(8);
            return view('crudRedesSociales.listaRedesSociales', compact('socialnetworks'));  
        } 
    }

        
    /**
     * create
     * Retorna al formulario para agregar redes sociales, hay iconos pre-agregados en la BD a través de Seeders
     * @author David Navarrete
     * @return void
     */
    public function create()
    {
        $icons = Icon::all()->sortBy('name');
        return view('crudRedesSociales.agregarRedSocial', compact('icons'));
    }

       
    /**
     * store
     * Guarda una red social
     * @param  mixed $request HTTP Rquest
     * @see StoreSocialRequest Validaciones
     * @author David Navarrete
     * @return void
     */
    public function store(StoreSocialRequest $request)
    {
        $socialnetwork = new Social();
        $socialnetwork->icon_id = trim($request->get('icon_id'));
        $socialnetwork->slug = Str::slug(trim($socialnetwork->icon->name));
        $socialnetwork->link = trim($request->get('link'));
        $socialnetwork->name_profile = trim($request->get('name_profile'));
        $socialnetwork->save();
        return redirect()->route('redes')->with('status', 'Red social agregada exitosamente'); 
    }

        
    /**
     * edit
     * Retorna al formulario para editar una red social
     * @param  object $rede Clase Social
     * @author David Navarrete
     * @return void
     */
    public function edit(Social $rede)
    {
        $socialnetwork = $rede;
        $icons = Icon::all()->sortBy('name');
        return view('crudRedesSociales.editarRedSocial', compact('socialnetwork', 'icons'));
    }

        
    /**
     * update
     * Actualiza una red social
     * @param  mixed $request HTTP Request
     * @see UpdateSocialRequest Validaciones
     * @param  object $rede Clase Social
     * @author David Navarrete
     * @return void
     */
    public function update(UpdateSocialRequest $request, Social $rede)
    {
        $socialnetwork = $rede;
        $socialnetwork->slug = Str::slug(trim($socialnetwork->icon->name));
        $socialnetwork->fill($request->all());
        $socialnetwork->save();
        return redirect()->route('redes')->with('status', 'Red social editada exitosamente');
    }

        
    /**
     * destroy
     * Elimina una red social
     * @param  object $rede Clase Social
     * @author David Navarrete
     * @return void
     */
    public function destroy(Social $rede)
    {  
        $socialnetwork = $rede;
        $socialnetwork->delete();
        return redirect()->route('redes')->with('status', 'Red social eliminada exitosamente');
    }
}
