<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Information;
use App\Social;

class UsController extends Controller
{
        
    /**
     * index
     * Muestra la vista de nosotros
     * @link https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3893.6745672704283!2d-72.03064!3d-36.799108!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf71cb509fe1f8017!2sFerreteria%20San%20Felipe!5e1!3m2!1ses-419!2scl!4v1579060705500!5m2!1ses-419!2scl
     * @author David Navarrete
     * @return void
     */
    public function index()
    {
        $informations = Information::all();
        $socials = Social::with('icon')->get();
        $n_socials = [];
        array_push($n_socials, (object)['name_profile' => 'Ferretería San Felipe', 'icon' => 'fab fa-facebook-square', 'link' => 'https://www.google.com']);
        array_push($n_socials, (object)['name_profile' => '+569 44444444', 'icon' => 'fab fa-whatsapp', 'link' => 'https://www.google.com']);
        $n_informations = [];
        array_push($n_informations, (object)['ubication' => 'Nuestra ubicación', 'email' => 'contacto@ferreteriasanfelipe.cl', 'normal' => '09:00 - 18:00', 'sunday' => '11:00 - 15:00', 'holidays' => '10:00 - 13:00']);
        $ubicationMap = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3893.6745672704283!2d-72.03064!3d-36.799108!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf71cb509fe1f8017!2sFerreteria%20San%20Felipe!5e1!3m2!1ses-419!2scl!4v1579060705500!5m2!1ses-419!2scl";
        return view('nosotros', compact('informations', 'socials', 'ubicationMap', 'n_socials', 'n_informations'));
    }
}
