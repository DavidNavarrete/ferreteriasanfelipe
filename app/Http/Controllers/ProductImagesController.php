<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImages;
use App\Http\Requests\StoreProductImagesRequest;
use App\Http\Requests\UpdateProductImagesRequest;
use Image;
use File;
use JD\Cloudder\Facades\Cloudder;

class ProductImagesController extends Controller
{
        
    /**
     * index
     * Muestra las imagenes relacionas a un producto (imagenes opcionales)
     * @param  mixed $request HTTP Request buscador
     * @author David Navarrete
     * @return void
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $imagesproduct = ProductImages::with('product')->whereHas('product', function($query2) use ($request){
              $query2->where('name', 'LIKE', '%'.trim($request->get('search')).'%');
            })->orderBy('product_id')->paginate(3);
            return view('crudProductoImagenes.listaImagenesProducto', compact('imagesproduct', 'query'));
        }else{
            $imagesproduct = ProductImages::with('product')->orderBy('product_id')->paginate(3);
            return view('crudProductoImagenes.listaImagenesProducto', compact('imagesproduct'));
        }
    }

        
    /**
     * create
     * Retorna al formulario para agregar imagenes opcionales
     * @author David Navarrete
     * @return void
     */
    public function create()
    {
        $products = Product::all();
        return view('crudProductoImagenes.agregarImagenesProducto', compact('products'));
    }

            
    /**
     * store
     * Guarda las imagenes opcionales del producto, puede guardar 1, 2 o 3 según quiera,
     * los tamaños de las imagenes se guardan en 300x300
     * @param  mixed $request HTTP Request
     * @see StoreProductImagesRequest Validaciones
     * @see Cludder
     * @see https://github.com/Intervention/image
     * @author David Navarrete
     * @return void
     */
    public function store(StoreProductImagesRequest $request)
    {
        $imagesproduct = new ProductImages();
        if($request->hasFile('image1', 'image2', 'image3')){

            $file = $request->file('image1');
            $file2 = $request->file('image2');
            $file3 = $request->file('image3');
            $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
                $c->aspectRatio();
            });
            $name_img = time().$file->getClientOriginalName();
            $path = public_path().'/images/productos/adicionales/'.$name_img;
            Cloudder::upload($file->getRealPath(), null);
            $imageCloud = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
            $imageResize->save($path);
            if($file2 == null){
                $name_img2 = NULL;
                $imageCloud2 = NULL;
            }else{
                $imageResize2 = Image::make($file2->getRealPath())->resize(300, 300, function($c){
                    $c->aspectRatio();
                });
                $name_img2 = time().$file2->getClientOriginalName();
                $path = public_path().'/images/productos/adicionales/'.$name_img2;
                Cloudder::upload($file2->getRealPath(), null);
                $imageCloud2 = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
                $imageResize2->save($path);
            }
            if($file3 == null){
                $name_img3 = NULL;
                $imageCloud3 = NULL;
            }else{
                $imageResize3 = Image::make($file3->getRealPath())->resize(300, 300, function($c){
                    $c->aspectRatio();
                });
                $name_img3 = time().$file3->getClientOriginalName();
                $path = public_path().'/images/productos/adicionales/'.$name_img3;
                Cloudder::upload($file3->getRealPath(), null);
                $imageCloud3 = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
                $imageResize3->save($path);
            }
        }
        //antes = $imagesproduct->image = $name_img;
        $imagesproduct->image1 = $imageCloud;
        $imagesproduct->image2 = $imageCloud2;
        $imagesproduct->image3 = $imageCloud3;
        $imagesproduct->product_id = trim($request->get('product_id'));
        $imagesproduct->save();
        //redireccionamos a la vista principal
        return redirect()->route('imagenes')->with('status', 'Imagenes agregadas exitosamente');
    }


        
    /**
     * edit
     * Retorna al formulario para agregar imagenes opcionales al producto
     * @param  object $imagene Clase ProductImages Imagenes del producto
     * @author David Navarrete
     * @return void
     */
    public function edit(ProductImages $imagene)
    {
        $images = $imagene;
        $products = Product::all()->sortBy('name');     
        return view('crudProductoImagenes.editarImagenesProducto', compact('images', 'products'));

    }

        
    /**
     * update
     * Actualiza las imagenes opcionales del producto
     * @param mixed $request HTTP Request
     * @param  object $imagene Clase ProductImages 
     * @see UpdateProductImagesRequest Validaciones
     * @see Cludder
     * @see https://github.com/Intervention/image
     * @author David Navarrete
     * @return void
     */
    public function update(UpdateProductImagesRequest $request, ProductImages $imagene)
    {
        $images = $imagene;
        $name_img = $images->image1;
        $name_img2 = $images->image2;
        $name_img3 = $images->image3;
   
         if($request->hasFile('image1')){
                $file_path = public_path() . '/images/productos/adicionales/' . $images->image1;
                File::delete($file_path);
                $file = $request->file('image1');
                $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
                    $c->aspectRatio();
                });
                $name_img = time().$file->getClientOriginalName();
                $path = public_path().'/images/productos/adicionales/'.$name_img;
                Cloudder::upload($file->getRealPath(), null);
                $imageCloud = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
                $imageResize->save($path);
         }else{
             $imageCloud = $name_img;
         }
         if($request->hasFile('image2')){
                 $file_path2 = public_path() . '/images/productos/adicionales/' . $images->image2;
                File::delete($file_path2);
                $file2 = $request->file('image2');
                $imageResize2 = Image::make($file2->getRealPath())->resize(300, 300, function($c){
                    $c->aspectRatio();
                });
                $name_img2 = time().$file2->getClientOriginalName();
                $path = public_path().'/images/productos/adicionales/'.$name_img2;
                Cloudder::upload($file2->getRealPath(), null);
                $imageCloud2 = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
                $imageResize2->save($path);
         }else{
            $imageCloud2 = $name_img2;
         }
         if($request->hasFile('image3')){
                $file_path3 = public_path() . '/images/productos/adicionales/' . $images->image3;
                File::delete($file_path3);
                $file3 = $request->file('image3');
                $imageResize3 = Image::make($file3->getRealPath())->resize(300, 300, function($c){
                    $c->aspectRatio();
                });
                $name_img3 = time().$file3->getClientOriginalName();
                $path = public_path().'/images/productos/adicionales/'.$name_img3;
                Cloudder::upload($file3->getRealPath(), null);
                $imageCloud3 = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
                $imageResize3->save($path);
         }else{
            $imageCloud3 = $name_img3;
         }
        //  antes = $images->image1 = $name_img;
         $images->image1 = $imageCloud;
         $images->image2 = $imageCloud2;
         $images->image3 = $imageCloud3;
         $images->product_id = trim($request->get('product_id'));
         $images->save();
         //redireccionamos a la vista principal
         return redirect()->route('imagenes')->with('status', 'Imagenes editadas exitosamente');
    }

        
    /**
     * destroy
     * Elimina todas las imagenes opcionales del producto
     * @param  object $imagene Clase ProductImages
     * @see File
     * @author David Navarrete
     * @return void
     */
    public function destroy(ProductImages $imagene)
    {
        $images = $imagene;
        $file_path = public_path() . '/images/productos/adicionales/' . $images->image1;
        File::delete($file_path);
        $file_path2 = public_path() . '/images/productos/adicionales/' . $images->image2;
        File::delete($file_path2);
        $file_path3 = public_path() . '/images/productos/adicionales/' . $images->image3;
        File::delete($file_path3);
        $images->delete();
        return redirect()->route('imagenes')->with('status', 'Imagenes eliminadas exitosamente');
    }
}
