<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInformationRequest;
use App\Information;

class InformationController extends Controller
{
        
    /**
     * index
     * Retorna a la vista información de la empresa
     * @author David Navarrete
     * @return void
     */
    public function index()
    {
        $information = Information::all();
        return view('crudInformacion.listaInformacion', compact('information'));
    }

        
    /**
     * create
     *  Retorna al formulario para agregar información
     * @author David Navarrete
     * @return void
     */
    public function create()
    {
        return view('crudInformacion.agregarInformacion');
    }

        
    /**
     * store
     *  Guarda información agregada en el formulario
     * si ya existe un registro, no se puede agregar otro, solo editar
     * @param  mixed $request HTTP Request 
     * @see StoreInformationRequest Validaciones
     * @author David Navarrete
     * @return void
     */
    public function store(StoreInformationRequest $request)
    {
        $normal_working_hours_start = trim($request->get('normal_working_hours_start'));
        $normal_working_hours_end = trim($request->get('normal_working_hours_end'));
        $normal_working_hours = $normal_working_hours_start.' a '.$normal_working_hours_end;
        $working_hours_sunday_start = trim($request->get('working_hours_sunday_start'));
        $working_hours_sunday_end = trim($request->get('normal_working_hours_end'));
        $working_hours_sunday = $working_hours_sunday_start.' a '.$working_hours_sunday_end;
        $holidays_working_hours_start = trim($request->get('holidays_working_hours_start'));
        $holidays_working_hours_end = trim($request->get('holidays_working_hours_end'));
        $holidays_working_hours = $holidays_working_hours_start.' a '.$holidays_working_hours_end;
        $information = new Information();
        $information->normal_working_hours = $normal_working_hours;
        $information->working_hours_sunday = $working_hours_sunday;
        $information->holidays_working_hours = $holidays_working_hours;
        $information->mission = trim($request->get('mission'));
        $information->vision = trim($request->get('vision'));
        $information->description = trim($request->get('description'));
        $information->ubication = trim($request->get('ubication'));
        $information->id = 1;
        $count = $information->count();
        if($count == 0){
            $information->id = 1;
            $information->save();
            return redirect()->route('informacion')->with('status', 'Información agregada exitosamente');
        }else{
            return redirect()->route('informacion')->with('status', 'Ya existe un registro');
        }
    }

        
    /**
     * edit
     * Retorna al formulario para editar la información de la empresa
     * @param  object $informacion Clase Information
     * @return void
     */
    public function edit(Information $informacion)
    {
        $information = $informacion;
        $split_normal_working_hours = explode(' a ', $information->normal_working_hours, 2);
        $normal_working_hours_start = $split_normal_working_hours[0];
        $normal_working_hours_end = $split_normal_working_hours[1];
        $split_working_hours_sunday = explode(' a ', $information->working_hours_sunday, 2);
        $working_hours_sunday_start = $split_working_hours_sunday[0];
        $working_hours_sunday_end = $split_working_hours_sunday[1];
        $split_holidays_working_hours = explode(' a ', $information->holidays_working_hours, 2);
        $holidays_working_hours_start = $split_holidays_working_hours[0];
        $holidays_working_hours_end = $split_holidays_working_hours[1];
        
       return view('crudInformacion.editarInformacion', compact('information', 'normal_working_hours_start',
        'normal_working_hours_end', 'working_hours_sunday_start', 'working_hours_sunday_end', 'holidays_working_hours_start',
        'holidays_working_hours_end'));
    }

    /**
     * update
     * Actualiza la información de la empreasa
     * @param  mixed $request HTTP Request
     * @see StoreInformationRequest Validaciones
     * @param  object $informacion Clase Information
     * @author David Navarrete
     * @return void
     */
    public function update(StoreInformationRequest $request, Information $informacion)
    {
        $information = $informacion;
        $normal_working_hours_start = trim($request->get('normal_working_hours_start'));
        $normal_working_hours_end = trim($request->get('normal_working_hours_end'));
        $normal_working_hours = $normal_working_hours_start.' a '.$normal_working_hours_end;
        $working_hours_sunday_start = trim($request->get('working_hours_sunday_start'));
        $working_hours_sunday_end = trim($request->get('normal_working_hours_end'));
        $working_hours_sunday = $working_hours_sunday_start.' a '.$working_hours_sunday_end;
        $holidays_working_hours_start = trim($request->get('holidays_working_hours_start'));
        $holidays_working_hours_end = trim($request->get('holidays_working_hours_end'));
        $holidays_working_hours = $holidays_working_hours_start.' a '.$holidays_working_hours_end;
        $information->normal_working_hours = $normal_working_hours;
        $information->working_hours_sunday = $working_hours_sunday;
        $information->holidays_working_hours = $holidays_working_hours;
        $information->fill($request->all());
        $information->save();
        return redirect()->route('informacion')->with('status', 'Información editada exitosamente');
    }

}
