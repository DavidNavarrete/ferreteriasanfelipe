<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use \Cart;
use \Validator;
use App\Information;
use App\Social;
use \Toastr;
use \Auth;

class CartController extends Controller
{    
    /**
     * index
     * Muestra los productos agregados al carro por el usuario
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function index(){
        //carro sin autenticar
        $cartItemsWithoutRegister = Cart::getContent();
        //si el usuario esta logeado
        if(!Auth::guest()){ 
            //guardo los datos en un carro nuevo con los datos del antiguo
            $cartItems = Cart::getContent($cartItemsWithoutRegister);
            $total = Cart::getTotal();
        }
        $socials = Social::with('icon')->orderBy('name_profile')->get();
        $informations = Information::all();
        $n_socials = [];
        array_push($n_socials, (object)['name_profile' => 'Ferretería San Felipe', 'icon' => 'fab fa-facebook-square', 'link' => 'https://www.google.com']);
        array_push($n_socials, (object)['name_profile' => '+569 44444444', 'icon' => 'fab fa-whatsapp', 'link' => 'https://www.google.com']);
        $n_informations = [];
        array_push($n_informations, (object)['ubication' => 'Nuestra ubicación', 'email' => 'contacto@ferreteriasanfelipe.cl', 'normal' => '09:00 - 18:00', 'sunday' => '11:00 - 15:00', 'holidays' => '10:00 - 13:00']);
        return view('carro', compact('cartItems', 'socials', 'informations', 'total', 'n_socials', 'n_informations'));
    }    

    /**
     * add
     * Agrega los productos al carro de compras
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @see https://github.com/brian2694/laravel-toastr
     * @author David Navarrete - Sergio Lagos
     * @param  object $product Clase Producto
     * @return void
     */
    public function add(Product $product){
        Cart::add(array(
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'quantity' => 1,
            'attributes' => array(),
            'associatedModel' => $product
        ));
        Toastr::success('El producto se agregó a tu carro de compras, inicia sesión o regístrate para ver los productos agregados, si ya iniciaste sesión, haz click en el ícono del carro.','Agregado', ['progressBar' => true, 'timeOut' => '10009']);
        return back();
    }
        
    /**
     * confirmStock
     * Función que permite confirmar si el stock de un producto es menor a la cantidad solicitad
     * @author David Navarrete - Sergio Lagos
     * @param  integer $productId id del producto
     * @param  integer $quantity cantidad solicitada por usuario
     * @return boolean
     */
    public function confirmStock($productId, $quantity){
        $product = Product::where('id', '=', $productId)->pluck('stock')->first();
        if($product < $quantity){
            return false;
        }else{
            return true;
        }
    }
    /**
     * update
     * Actualiza un producto dentro del carro de compras
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @see https://github.com/brian2694/laravel-toastr
     * @author David Navarrete - Sergio Lagos
     * @param  integer $rowId Id del producto
     * @return void
     */
    public function update($rowId, Request $request){
        
        if($this->confirmStock($rowId, $request->get('quantity')) == false){
            Toastr::info('No contamos con el stock solicitado','Información');
            return back();
        }else{
        Cart::update($rowId, [
            'quantity'=> array(
                'relative' => false, 
                'value' => request('quantity')
            )
        ]);
        Toastr::success('El producto se actualizó correctamente.','Actualizado');
        return back();
        }
    }
    
    /**
     * destroy
     * Elimina un producto del carro de compras
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @see https://github.com/brian2694/laravel-toastr
     * @param  integer $itemId ID del producto
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function destroy($itemId){
        Cart::remove($itemId);
        Toastr::success('El producto se eliminó correctamente.','Eliminado');
        return back();
    }    
    /**
     * clear
     * Limpia todos los productos del carro de compras
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @see https://github.com/brian2694/laravel-toastr
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function clear(){
        Cart::clear();
        Toastr::success('El carro de compras quedo vacío.','Limpio');
        return back();
    }
}
