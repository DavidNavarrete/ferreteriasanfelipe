<?php

namespace App\Http\Controllers;

use Freshwork\Transbank\WebpayNormal;
use Freshwork\Transbank\WebpayPatPass;
use Freshwork\Transbank\CertificationBag;  
use Freshwork\Transbank\CertificationBagFactory;  
use Freshwork\Transbank\TransbankServiceFactory;  
use Freshwork\Transbank\RedirectorHelper;  
use Illuminate\Http\Request;
use App\Buy;
use \Auth;
use \Cart;
use App\MyCart;
use App\Product;
use App\User;
use \DB;

class CheckoutController extends Controller
{
	
		
	/**
	 * compareOrder
	 * Función que compara las ordenes en la base de datos con la orden generada aleatoriamente
	 * @return string orden de compra
	 */
	public function compareOrder(){
		$buyOrder = Buy::pluck('order')->first();
		$orderRandom = 'Orden-' . rand(1000, 9999);
		foreach((array) $buyOrder as $order){
			if($order == $orderRandom){
				$orderRandom = 'Orden-' . rand(1000, 9999);
			}
		}
		return $orderRandom;
	}
    /**
     * initTransaction
	 * Inicia la transacción para trabajar con Transbank enviando el total a pagar y el id del usuario
     *
     * @param  object $webpayNormal WebpayNormal
     * @param  integer $total Cantidad a pagar
     * @param  integer $id id del usuario
	 * @see https://github.com/freshworkstudio/transbank-web-services
	 * @author David Navarrete
     * @return void
     */
    public function initTransaction(WebpayNormal $webpayNormal, $total, $id)
	{
		$bag = CertificationBagFactory::integrationWebpayNormal(); 
		$webpayNormal = TransbankServiceFactory::normal($bag);
		//agrega la transacción y toma como parametros el total a pagar y un número random de orden de compra.
		$webpayNormal->addTransactionDetail($total, $this->compareOrder()); 
		$cartItems = Cart::getContent();
		foreach($cartItems as $item){
			$cart = new MyCart();
			$cart->product_id = $item->id;
			$cart->user_id = Auth::User()->id;
			$cart->quantity = $item->quantity;
			$cart->save();
		}
        $response = $webpayNormal->initTransaction(route('checkout.webpay.response'), route('checkout.webpay.finish'), $id); 
		// Probablemente también quieras crear una orden o transacción en tu base de datos y guardar el token ahí.
        return RedirectorHelper::redirectHTML($response->url, $response->token);
	}
	
	/**
	 * saveBuyUsers
	 * Guarda las compras en la tabla pivot buy_user para guardar las compras de los usuarios
	 * @param array $myCart Datos del carro de compras
	 * @param string $order Orden de compra
	 * @author David Navarrete - Sergio Lagos
	 * @return void
	 */
	private function saveBuyUsers($myCart){
		//ultima id de compra
		$buyId = Buy::latest('id')->pluck('id')->first();
		$product = new Product();
		foreach($myCart as $item){
			$product->buys()->attach($item->product_id, ['quantity' => $item->quantity, 'buy_id' => $buyId]);
			$product = Product::find($item->product_id);
			$product->update(['stock' => $product->stock - $item->quantity]);
		}
		$this->deleteDataMyCart();
	}	
	/**
	 * response
	 *	Redirecciona al formulario para pagar y generar el detalle de la compra
	 * @see https://github.com/freshworkstudio/transbank-web-services
	 * @author David Navarrete
	 * @return void
	 */
	public function response()  
	{  	
		$bag = CertificationBagFactory::integrationPatPass(); 
		$webpayPatPass = TransbankServiceFactory::patpass($bag);
		$result = $webpayPatPass->getTransactionResult();  
		$cartItems = MyCart::where('user_id', '=', $result->sessionId)->get();
	  // Revisar si la transacción fue exitosa ($result->detailOutput->responseCode === 0) o fallida para guardar ese resultado en tu base de datos. 
	  if($result->detailOutput->responseCode === 0){
		$buy = new Buy();
		$buy->total = $result->detailOutput->amount;
		$buy->user_id = $result->sessionId;
		$buy->order = $result->buyOrder;
		$buy->status = 'Lista para retiro';
		$buy->save();
		$this->saveBuyUsers($cartItems);
		$user = User::where('id', '=', $result->sessionId)->get();
		$this->sendEmailOrderBuy($user);
	  }
	  $webpayPatPass->acknowledgeTransaction();  
	  return RedirectorHelper::redirectBackNormal($result->urlRedirection);  
	}	
	
	/**
	 * discountStockProduct
	 * Descuenta el stock de un producto restandolo con la cantidad comprada
	 * @author David Navarrete - Sergio Lagos
	 * @param  mixed $id id del producto
	 * @param  mixed $quantity cantidad a comprar
	 * @return void
	 */
	public function discountStockProduct($id, $quantity){
		$product = Product::where('id', '=', $id)->get();
		foreach($product as $p){
		$p->stock = $p->stock - $quantity;
		$p->save();
		}
	}	
		/**
		 * deleteDataMyCart
		 * elimina los datos de la tabla dinamica "my_carts"
		 * @author David Navarrete - Sergio Lagos
		 * @return void
		 */
		public function deleteDataMyCart(){
			$dataCarts = MyCart::all();
			foreach($dataCarts as $data){
				$data->delete();
			}
		}	
	/**
	 * sendEmailOrderBuy
	 * envía un correo al usuario con un documento en PDF de su compra
	 * @author David Navarrete - Sergio Lagos
	 * @param  mixed $user Usuario
	 * @return void
	 */
	public function sendEmailOrderBuy($user){
		$userId = $user->pluck('id')->first();
		$buyId = Buy::latest('id')->pluck('id')->first();
		$buyOrder = Buy::latest('order')->pluck('order')->first();
		$details = DB::table('buy_user as bu')->select('p.name as pname', 'p.image as pimage', 'bu.quantity as bquantity', 'u.id as uid', 'u.name as uname', 'u.lastname as ulastname', 'u.email as uemail', 'b.order as border', 'b.total as btotal')
		->join('products as p', 'bu.product_id', '=', 'p.id')
		->join('buys as b', 'bu.buy_id', '=', 'b.id')->join('users as u', 'b.user_id', '=', 'u.id')
		->where('b.user_id', '=', $userId)
		->where('b.order', '=', $buyOrder)
		->where('bu.buy_id', '=', $buyId)->get();
		$total = $details->pluck('btotal')->first();
		\Mail::to($user->pluck('email')->first())->send(new \App\Mail\sendMail($details, $user, $total));
	}
	/**
	 * finish
	 * Redirecciona al usuario a la página principal
	 * @see https://github.com/freshworkstudio/transbank-web-services
	 * @author David Navarrete - Sergio Lagos
	 * @return void
	 */
	public function finish()  
	{
		return redirect()->route('principal');
	}
	
}
