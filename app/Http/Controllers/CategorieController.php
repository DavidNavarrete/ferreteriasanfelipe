<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategorieRequest;
use Illuminate\Support\Str;

class CategorieController extends Controller
{
    /**
     * index
     * Muestra la página principal de las categorías
     *
     * @param  mixed $request HTTP Request buscador
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('searchcategorie'));
            $categories = Categorie::with('subcategories')->whereHas('subcategories', function($query2) use ($request){
                $query2->where('name', 'LIKE', '%'.trim($request->get('searchcategorie')).'%');
            })->orWhere('name', 'LIKE', '%'.$query.'%')->orderBy('name')->paginate(8);
            return view('crudCategoria.listaCategoria', compact('categories', 'query'));
        }else{
            $categories = Categorie::with('subcategories')->orderBy('name')->paginate(8);
            return view('crudCategoria.listaCategoria', compact('categories'));
        }
    }

        
    /**
     * create
     * Retorna al formulario para crear una categoría
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function create()
    {
        return view('crudCategoria.agregarCategoria');
    }

        
    /**
     * store
     * Guarda una categoría
     * @param  mixed $request HTTP Request
     * @see StoreCategorieRequest Validaciones
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function store(StoreCategorieRequest $request)
    {
        $categorie = new Categorie();
        $categorie->name = trim($request->input('name'));
        $categorie->slug = Str::slug(trim($request->name));
        $categorie->save();
        return redirect()->route('categorias')->with('status', 'Categoría agregada exitosamente');
    }
        
    /**
     * edit
     * Retorna al formulario para editar la categoría
     * @param  object $categoria Clase Categoría
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function edit(Categorie $categoria)
    {
        $categorie = $categoria;
        return view('crudCategoria.editarCategoria', compact('categorie'));
    }

        
    /**
     * update
     * Actualiza la categoría
     * @param  mixed $request HTTP Request
     * @see StoreCategorieRequest Validaciones
     * @param  object $categoria Clase Categoría
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function update(StoreCategorieRequest $request, Categorie $categoria)
    {
        $categorie = $categoria;
        $categorie->fill($request->all());
        $categorie->save();
        return redirect()->route('categorias')->with('status', 'Categoría editada exitosamente');
    }

        
    /**
     * destroy
     * Elimina una categoría
     * Si la categoría está relacionada con otras subcategorías, no se podrá eliminar
     * se debe eliminar la subcategoría hija primero
     * @param  object $categoria clase Categoría
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function destroy(Categorie $categoria)
    {
        $categorie = $categoria;
        try{
            $categorie->delete();
            return redirect()->route('categorias')->with('status', 'Categoría eliminada exitosamente');
            
        }catch(\Illuminate\Database\QueryException $e){
            if($e->getCode() == 23000){
                return redirect()->route('categorias')->with('status', 'Esta categoría está relacionada a una o varias subcategorías, eliminelas y vuelva a intentar');
                
            }
        }
    }
}
