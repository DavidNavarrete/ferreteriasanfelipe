<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buy;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

     
    /**
     * index
     * Retorna la vista principal del panel de administración mostrando las ganancias generadas por día, mes y año
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function index()
    {
        $earningsToday = Buy::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))->sum('total');
        $earningsMonth = Buy::whereMonth('created_at', '=', Carbon::now())->sum('total');
        $earningsYear = Buy::whereYear('created_at', '=', Carbon::now())->sum('total');
        $month = Carbon::now();
        return view('home', compact('earningsToday', 'earningsMonth', 'earningsYear', 'month'));
    }
}
