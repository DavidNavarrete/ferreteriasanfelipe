<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \PDF;
use \Auth;
use \Cart;
use App\User;

class PDFController extends Controller
{    
    /**
     * printPDF
     * Genera el pdf de los productos del carro de compras del usuario
     * @see https://github.com/carlosgraterol/Dompdf-Laravel7
     * @see https://github.com/darryldecode/laravelshoppingcart
     * @author David Navarrete - Sergio Lagos
     * @return void Vista previa del archivo PDF
     */
    public function printPDF(){
        //tomo los datos del carrito de compras del usuario
        $cartItems = \Cart::getContent();
        $total = Cart::getTotal();
        //cargo los datos a la vista y paso los datos del carrito
        $user = User::where('id', '=', Auth::User()->id);
        $pdf = PDF::loadView('cotizacionPDF', compact('cartItems', 'total', 'user'));
        //retorno una vista previa del archivo y su nombre al descargarlo sera cotizacion.pdf
        return $pdf->stream('cotizacion.pdf');
    }
}
