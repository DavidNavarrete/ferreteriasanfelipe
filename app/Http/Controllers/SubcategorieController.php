<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSubcategorieRequest;
use App\Http\Requests\UpdateSubcategorieRequest;
use App\Categorie;
use App\Subcategorie;
use Illuminate\Support\Str;


class SubcategorieController extends Controller
{
        
    /**
     * index
     * Muestra la vista principal de las subcategorías
     * @param  mixed $request HTTP Request buscador
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('searchsubcategorie'));
            $subcategories = Subcategorie::with('categorie')->whereHas('categorie', function ($query2) use ($request) {
                $query2->where('name', 'LIKE', '%' . trim($request->get('searchsubcategorie')) . '%');
            })->orWhere('name', 'LIKE', '%' . $query . '%')->orderBy('name')->paginate(8);
            return view('crudSubcategoria.listaSubcategoria', compact('subcategories', 'query'));
        } else {
            $subcategories = Subcategorie::with('categorie')->orderBy('name')->paginate(8);
            return view('crudSubcategoria.listaSubcategoria', compact('subcategories'));
        }
    }

        
    /**
     * create
     * Retorna al formulario para agregar una subcategoría
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function create()
    {
        $categories = Categorie::all()->sortBy('name');
        return view('crudSubcategoria.agregarSubcategoria', compact('categories'));
    }

        
    /**
     * store
     * Guarda una subcategoría
     * @param  mixed $request HTTP Request
     * @see StoreSubcategorieRequest Validaciones
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function store(StoreSubcategorieRequest $request)
    {
        $subcategorie = new Subcategorie();
        $subcategorie->name = trim($request->input('name'));
        $subcategorie->slug = Str::slug(trim($request->name));
        $subcategorie->categorie_id = trim($request->get('categorie_id'));
        $subcategorie->save();
        return redirect()->route('subcategorias')->with('status', 'Subcategoría agregada exitosamente');
    }

        
    /**
     * edit
     * Retorna al formulario para editar una subcategoría
     * @param  object $subcategoria Clase Subcategorie
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function edit(Subcategorie $subcategoria)
    {
        $subcategorie = $subcategoria;
        $categories = Categorie::all()->sortBy('name');
        return view('crudSubcategoria.editarSubcategoria', compact('subcategorie', 'categories'));
    }

        
    /**
     * update
     * Actualiza una subcategoría
     * @param  mixed $request HTTP Request
     * @see UpdateSubcategorieRequest Validaciones
     * @param  object $subcategoria Clase Subcategorie
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function update(UpdateSubcategorieRequest $request, Subcategorie $subcategoria)
    {
        $subcategorie = $subcategoria;
        $subcategorie->fill($request->all());
        $subcategorie->save();
        return redirect()->route('subcategorias')->with('status', 'Subcategoría editada exitosamente');
    }

        
    /**
     * destroy
     * Elimina una subcategoría, si la subcategroría está relacionada con uno o varios productos, se debe eliminar el producto y luego
     * la subcategoría
     * @param  object $subcategoria Clase Subcategorie
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function destroy(Subcategorie $subcategoria)
    {
        try {
            $subcategorie = $subcategoria;
            $subcategorie->delete();
            return redirect()->route('subcategorias')->with('status', 'Subcategoría eliminada exitosamente');
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == 23000) {
                return redirect()->route('subcategorias')->with('status', 'Esta subcategoría está relacionada con uno o varios productos, eliminelos y vuelva a intentar');
            }
        }
    }
}
