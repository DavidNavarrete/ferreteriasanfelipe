<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Role;
use \Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    //función para redireccionar
    public function redirectTo(){
        //tomo el id de rol de administrador
        $roleAdmin = Role::where('name', 'Admin')->pluck('id')->first();
        //pregunto si el usuario que se esta autenticando tiene rol de admin
        if(Auth::User()->role_id == $roleAdmin){
            //si tiene rol de admin, se le envía al home
            return url('home');
        }else{
            //si no tiene rol de admin, se le envía a la raíz
            return url('/');
        }
    }
}
