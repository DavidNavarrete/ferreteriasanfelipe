<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;
use \Auth;
use App\Information;
use App\Social;
use App\Buy;

class BuyController extends Controller
{
        
    /**
     * index
     * Muestra las compras realizadas por el usuario
     *
     * @return void
     * @author David Navarrete - Sergio Lagos
     */
    public function index(){
        $informations = Information::all();
        $socials = Social::with('icon')->get();
        $buys = DB::table('buys as b')
        ->select('b.id', 'b.order', 'b.status', 'b.created_at')
        ->join('buy_user as bu', 'b.id', '=', 'bu.buy_id')
        ->where('b.user_id', '=', Auth::User()->id)
        ->groupBy('b.order')
        ->get();
        $n_socials = [];
        array_push($n_socials, (object)['name_profile' => 'Ferretería San Felipe', 'icon' => 'fab fa-facebook-square', 'link' => 'https://www.google.com']);
        array_push($n_socials, (object)['name_profile' => '+569 44444444', 'icon' => 'fab fa-whatsapp', 'link' => 'https://www.google.com']);
       return view('compras', compact('buys', 'informations', 'socials', 'n_socials'));
    }    
    /**
     * show
     * Muestra los detalles de la orden de compra
     * @author David Navarrete - Sergio Lagos
     * @param  mixed $order orden de compra
     * @return void
     */
    public function show($order){
        
        $informations = Information::all();
        $socials = Social::with('icon')->get();
        $detailBuys = DB::table('buys as b')
        ->select('b.id', 'b.order', 'b.status', 'b.created_at', 'p.name', 'p.image', 'p.price', 'bu.quantity', 'b.user_id', 'b.total')
        ->join('buy_user as bu', 'b.id', '=', 'bu.buy_id')
        ->join('products as p', 'bu.product_id', '=', 'p.id')
        ->where(function($query){
            $query->where('b.user_id', '=', Auth::User()->id);
        })->where('b.order', '=', $order)
        ->get();
        $total = $detailBuys->pluck('total')->first();
        $n_socials = [];
        array_push($n_socials, (object)['name_profile' => 'Ferretería San Felipe', 'icon' => 'fab fa-facebook-square', 'link' => 'https://www.google.com']);
        array_push($n_socials, (object)['name_profile' => '+569 44444444', 'icon' => 'fab fa-whatsapp', 'link' => 'https://www.google.com']);
       return view('detalleCompra', compact('detailBuys', 'informations', 'socials', 'n_socials', 'total'));
    }
}
