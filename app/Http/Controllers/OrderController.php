<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Information;
use App\Social;
use \DB;
use App\Buy;

class OrderController extends Controller
{
    /**
     * Retorna a la vista de las ordenes de compra
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchorder'));
        if($query){
            $buys = DB::table('buys as b')
            ->select('b.id', 'b.order', 'b.status', 'b.created_at')
            ->join('buy_user as bu', 'b.id', '=', 'bu.buy_id')
            ->where('b.order', 'LIKE', '%'.$query.'%')
            ->groupBy('b.order')->orderByRaw('b.status DESC')
            ->get();
            return view('orden.index', compact('buys', 'query'));
        }else{
            return view('orden.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Actualiza un registro especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id id de la compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Buy::where('id',$id)->update(['status'=>trim($request->order)]);
        return redirect()->route('orden')->with('status', 'Estado modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
