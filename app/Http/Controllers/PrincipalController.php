<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Categorie;
use App\Social;
use App\Information;
use \Auth;

class PrincipalController extends Controller
{
        
    /**
     * index
     * Muestra la página principal del sitio
     * @param  mixed $request HTTP Request
     * @author David Navarrete - Sergio Lagos
     * @return void
     */
    public function index(Request $request)
    {
        $query = trim($request->get('search'));
        $categories = Categorie::with('subcategories')->orderBy('name')->get();
        $socials = Social::with('icon')->orderBy('name_profile')->get();
        $informations = Information::all();
        $n_categories = [];
        array_push($n_categories, (object)['categorie' => 'Categoría #1', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #2', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #3', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #4', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #5', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #6', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #7', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #8', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #9', 'subcategorie' => 'Subcategoría']);
        array_push($n_categories, (object)['categorie' => 'Categoría #10', 'subcategorie' => 'Subcategoría']);
        $n_products = [];
        array_push($n_products, (object)['name' => 'Producto #1', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #2', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #3', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #4', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #5', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #6', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_products, (object)['name' => 'Producto #7', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        $n_seasons = [];
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #1', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #2', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #3', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #4', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #5', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        array_push($n_seasons, (object)['name' => 'Primavera', 'season_image' => '/images/ofertaprimavera.jpg', 'name_product' => 'Producto #6', 'description' => 'Descripción del producto', 'price' => 5000, 'stock' => 5, 'updated_at' => '2020-09-20', 'slug' => 'name_product', 'image' => '/images/martillotest.jpg']);
        $n_socials = [];
        array_push($n_socials, (object)['name_profile' => 'Ferretería San Felipe', 'icon' => 'fab fa-facebook-square', 'link' => 'https://www.google.com']);
        array_push($n_socials, (object)['name_profile' => '+569 44444444', 'icon' => 'fab fa-whatsapp', 'link' => 'https://www.google.com']);
        $n_informations = [];
        array_push($n_informations, (object)['ubication' => 'Nuestra ubicación', 'email' => 'contacto@ferreteriasanfelipe.cl', 'normal' => '09:00 - 18:00', 'sunday' => '11:00 - 15:00', 'holidays' => '10:00 - 13:00']);
        if($query){
            $resultsproduct = Product::with('images')->with('season')->whereHas('subcategorie', function ($query2) use ($request) {
                $query2->where('name', 'like', '%'.$request->get('search').'%')
                ->orWhereHas('categorie', function ($query3) use ($request){
                    $query3->where('name', 'like', '%'.$request->get('search').'%');
                });
            })->orwhereHas('season', function ($query4) use ($request){
                $query4->where('name', 'like', '%'.$request->get('search').'%');
            })->orWhere('name','like', '%'.$request->get('search').'%')->orderBy('id')->paginate(16);
            return view('busqueda', compact('categories', 'socials', 'informations', 'resultsproduct', 'query', 'n_categories', 'n_products', 'n_seasons', 'n_socials', 'n_informations'));
        }else{
            $products_destacated = Product::with('images')->where('destacated', 'si')->orderBy('id')->get();
            $products = Product::with('images')->orderBy('id')->paginate(12);
            $products_season = Product::with('images')->whereHas('season', function($query4){
                $query4->where('enabled', '=', 'si');
            })->orderBy('id')->get();
            return view('principal', compact('products_destacated', 'categories', 'socials', 'informations', 'products', 'products_season', 'n_categories', 'n_products', 'n_seasons', 'n_socials', 'n_informations'));
        }
    } 

        
    /**
     * show
     * Muestra más información del producto
     * @param  integer $id ID del producto
     * @author David Navarrete
     * @return void
     */
    public function show($id)
    {
        $p = Product::find($id);
        return view('modalProductoDetalle', compact('p'));
    }

}
