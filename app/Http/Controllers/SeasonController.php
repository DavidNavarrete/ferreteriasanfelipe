<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Season;
use Illuminate\Support\Str;
use App\Http\Requests\StoreSeasonRequest;
use App\Http\Requests\UpdateSeasonRequest;
use File;
use Image;
use JD\Cloudder\Facades\Cloudder;

class SeasonController extends Controller
{
        
    /**
     * index
     * Muestra la vista principal de las temporadas
     * @param  mixed $request HTTP Request buscador
     * @author David Navarrete
     * @return void
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('searchseason'));
            $seasons = Season::where('name', 'LIKE', '%' . $query . '%')->orderBy('name')->paginate(4);
            return view('crudTemporada.listaTemporada', compact('seasons', 'query'));
        } else {
            $seasons = Season::all()->orderBy('name')->paginate(4);
            return view('crudTemporada.listaTemporada', compact('seasons'));
        }
    }

        
    /**
     * create
     * Retorna al formulario para crear una temporada
     * @author David Navarrete
     * @return void
     */
    public function create()
    {
        return view('crudTemporada.agregarTemporada');
    }

        
    /**
     * store
     * Guarda una temporada, las imagenes se guardan en un tamaño de 300x300
     * @param  mixed $request HTTP Request
     * @see StoreSeasonRequest Validaciones
     * @see Cloudder
     * @see https://github.com/Intervention/image
     * @author David Navarrete
     * @return void
     */
    public function store(StoreSeasonRequest $request)
    {
        $season = new Season();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
                $c->aspectRatio();
            });
            $name_img = time().$file->getClientOriginalName();
            $path = public_path().'/images/temporadas/'.$name_img;
            Cloudder::upload($file->getRealPath(), null);
            $imageCloud = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
            $imageResize->save($path);
        }
        $season = new Season();
        $season->name = trim($request->input('name'));
        $season->slug = Str::slug(trim($request->name));
        //antes = $season->image = $name_img;
        $season->image = $imageCloud;
        $season->enabled = trim($request->get('enabled'));
        $season->save();
        return redirect()->route('temporadas')->with('status', 'Temporada agregada exitosamente');
    }
 
    /**
     * edit
     * Retorna al formulario para editar la temporada
     * @param  object $temporada Clase Season
     * @author David Navarrete
     * @return void
     */
    public function edit(Season $temporada)
    {
        $season = $temporada;
        return view('crudTemporada.editarTemporada', compact('season'));
    }
    /**
     * update
     * Actualiza la temporada, las imagenes se guardan con un tamaño de 300x300
     * @param  mixed $request HTTP Request
     * @see UpdateSeasonRequest Validaciones
     * @see Cloudder
     * @see https://github.com/Intervention/image
     * @param  object $temporada Clase Season
     * @author David Navarrete
     * @return void
     */
    public function update(UpdateSeasonRequest $request, Season $temporada)
    {
        $season = $temporada;
        if ($request->hasFile('image')) {
            $file_path = public_path() . '/images/temporadas/' . $season->image;
            File::delete($file_path);
            $file = $request->file('image');
            $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
                $c->aspectRatio();
            });
            $name_img = time().$file->getClientOriginalName();
            $path = public_path().'/images/temporadas/'.$name_img;
            Cloudder::upload($file->getRealPath(), null);
            $imageCloud = Cloudder::show(Cloudder::getPublicId(), ["width" => 300, "height"=>300]);
            $imageResize->save($path);
            // antes= $season->image = $name_img;
            $season->image = $imageCloud;
        }
        $season->slug = Str::slug(trim($request->get('name')));
        $season->enabled = trim($request->get('enabled'));
        $season->fill($request->except('image'));
        $season->save();
        return redirect()->route('temporadas')->with('status', 'Temporada editada exitosamente');
    }

        
    /**
     * destroy
     * Elimina una temporada, si la temporada está asociada a un producto, se debe eliminar el producto hijo
     * y luego la temporada.
     * @param  object $temporada Clase Season
     * @see File
     * @author David Navarrete
     * @return void
     */
    public function destroy(Season $temporada)
    {
        try {
            $season = $temporada;
            $file_path = public_path() . '/images/temporadas/' . $season->image;
            $season->delete();
            File::delete($file_path);
            return redirect()->route('temporadas')->with('status', 'Temporada eliminada exitosamente');
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == 23000) {
                return redirect()->route('temporadas')->with('status', 'No se pudo eliminar esta temporada ya que está relacionada con uno o varios productos');
            }
        }
    }
}
