<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategorie;
use App\Season;
use App\Product;
use File;
use Image;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Str;
use JD\Cloudder\Facades\Cloudder;

class ProductController extends Controller
{
        
    /**
     * index
     * Muestra los productos
     * @param  mixed $request HTTP Requets buscador
     * @author David Navarrete
     * @return void
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('searchproduct'));
            $products = Product::where('name', 'LIKE', '%' . $query . '%')->orderBy('name')->paginate(8);
            return view('crudProducto.listaProducto', compact('products', 'query'));
        }else{
            $products = Product::all()->sortBy('name');
            return view('crudProducto.listaProducto', compact('products'));
        }  
    }

        
    /**
     * create
     * Retorna al formulario para agregar un producto
     * @author David Navarrete
     * @return void
     */
    public function create()
    {
        $subcategories = Subcategorie::all()->sortBy('name');
        $seasons = Season::all()->sortBy('name');
        return view('crudProducto.agregarProducto', compact('subcategories', 'seasons'));
    }

    /**
     * store
     * Guarda la información del producto, las imagenes se guardan con un tamaño de 300x300
     * @param  mixed $request StoreProductRequest Validaciones
     * @see StoreProductRequest Validaciones
     * @see https://github.com/Intervention/image
     * @see Cloudder
     * @author David Navarrete
     * @return void
     */
    public function store(StoreProductRequest $request)
    {
       if($request->hasFile('image')){
           $file = $request->file('image');
           $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
            $c->aspectRatio();
        });
        $name_img = time().$file->getClientOriginalName();
        $path = public_path().'/images/productos/'.$name_img;
        Cloudder::upload($file->getRealPath(), null);
        $imageCloud = Cloudder::show(Cloudder::getPublicId(),["width" =>300, "height"=>300]);
        $imageResize->save($path);
       }
        $products = new Product();
        $products->name = trim($request->get('name'));
        $products->slug = Str::slug(trim($request->get('name')));
        $products->description = trim($request->get('description'));
        $products->price = trim($request->get('price'));
        $products->stock = trim($request->get('stock'));
        $products->destacated = trim($request->get('destacated'));
        $products->image = $imageCloud;
        $products->subcategorie_id = trim($request->get('subcategorie_id'));
        $products->season_id = $request->get('season_id');
        $products->save();
       
       return redirect()->route('productos')->with('status', 'Producto agregado exitosamente');
    }

        
    /**
     * show
     * Muestra más información del producto
     * @param  object $producto Clase Producto
     * @author David Navarrete
     * @return void
     */
    public function show(Product $producto)
    {
        $product = $producto;
        $id = $product->id;
        $product = Product::where('id', '=', $id)->get();
        
       return view('crudProducto.mostrarProducto', compact('product'));
    }

        
    /**
     * edit
     * Retorna al formulario para editar un producto
     * @param  object $producto Clase Producto
     * @author David Navarrete
     * @return void
     */
    public function edit(Product $producto)
    {
        $product = $producto;
        $subcategories = Subcategorie::all()->sortBy('name');
        $seasons = Season::all()->sortBy('name');
        
        return view('crudProducto.editarProducto', compact('product', 'subcategories', 'seasons'));
    }

        
    /**
     * update
     * Actualiza un producto
     * @param  mixed $request HTTP Request
     * @see UpdateProductRequest Validaciones
     * @see File
     * @see https://github.com/Intervention/image
     * @see Cloudder
     * @param  object $producto Clase Producto
     * @author David Navarrete
     * @return void
     */
    public function update(UpdateProductRequest $request, Product $producto)
    {
        $product = $producto;
        if ($request->hasFile('image')) {
            $file_path = public_path() . '/images/productos/' . $product->image;
            File::delete($file_path);
            $file = $request->file('image');
            $imageResize = Image::make($file->getRealPath())->resize(300, 300, function($c){
                $c->aspectRatio();
            });
            $name_img = time().$file->getClientOriginalName();
            $path = public_path().'/images/productos/'.$name_img;
            Cloudder::upload($file->getRealPath(), null);
            $imageCloud = Cloudder::show(Cloudder::getPublicId(),["width" =>300, "height"=>300]);
            $imageResize->save($path);
            //antes = $product->image = $name_img;
            $product->image = $imageCloud;
        }
        $product->destacated = trim($request->get('destacated'));
        $product->fill($request->except('image'));
        $product->save();

        return redirect()->route('productos')->with('status', 'Producto editado exitosamente');
    }

        
    /**
     * destroy
     * Elimina un producto
     * Si el producto tiene una subcategoría relacionada o temporada, se deberá eliminar primero
     * las subcategorías o temporadas hijas
     * @param  object $producto Clase Producto
     * @see File
     * @author David Navarrete
     * @return void
     */
    public function destroy(Product $producto)
    {
        try{
        $product = $producto;
        $file_path = public_path() . '/images/productos/' . $product->image;
        $producto->delete();
        File::delete($file_path);
        return redirect()->route('productos')->with('status', 'Producto eliminado exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            if ($e->getCode() == 23000) {
                return redirect()->route('productos')->with('status', 'Este producto tiene una subcategoría o temporada relacionada, eliminelas y vuelva a intentar');
            }
        }
    }

}
