<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordNotification extends ResetPassword
{

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reestablecimiento de contraseña')
            ->greeting('Hola, ' . $notifiable->name)
            ->line('Este correo fue enviado porque solicitaste un reestablecimiento de contraseña de tu cuenta ' . $notifiable->getEmailForPasswordReset() . ', haz click en el botón que aparece a continuación para cambiar tu contraseña.')
            ->action('Reestablecer contraseña', route('password.reset', $this->token))
            ->line('Si no solicitó un restablecimiento de contraseña, no se requiere ninguna otra acción.')
            ->line('Este enlace solo es válido dentro de los proximos 60 minutos')
            ->salutation('¡Saludos!');
    }

}
