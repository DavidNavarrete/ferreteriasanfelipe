<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['icon_id', 'slug', 'link', 'name_profile'];
    /**
     * getRouteKeyName
     * Retorna el slug a la url
     * @author David Navarrete
     * @return string slug
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /**
     * social
     * Relacion de uno a uno entre Icono y Social
     * @author David Navarrete
     * @return void
     */
    public function icon()
    {
        return $this->belongsTo('App\Icon');
    }
}
