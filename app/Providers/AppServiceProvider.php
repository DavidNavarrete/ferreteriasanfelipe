<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setUTF8(true);
      Carbon::setLocale(config('app.locale'));
      setlocale(LC_TIME, config('app.locale'));
        //línea que arreglo los estilos en heroku,  verifica si está en modo de desarrollo o producción para renderizar sus activos.
        if($this->app->environment('production')) {
            \URL::forceScheme('https');
        }
        
    }
}
