<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategorie extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['name', 'slug', 'categorie_id'];
     /**
     * getRouteKeyName
     * Retorna el slug a la url
     * @author David Navarrete
     * @return string slug
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
        
    /**
     * categorie
     * Relación de uno a muchos entre categoría y subcategoría
     * @author David Navarrete
     * @return void
     */
    public function categorie(){
        return $this->belongsTo('App\Categorie');
    }    
    /**
     * products
     * Relación de uno a muchos entre producto y subcategoría
     * @author David Navarrete
     * @return void
     */
    public function products(){
        return $this->hasMany('App\Product');
    }
}
