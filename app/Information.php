<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['normal_working_hours', 'working_hours_sunday', 'holidays_working_hours', 'mission', 'vision', 
    'description', 'ubication'];
}
