<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    public $timestamps = false;

    protected $fillable = ['image1', 'image2', 'image3', 'product_id'];

       
    /**
     * product
     * Relación de uno a muchos entre producto y producto-imagenes
     * @author David Navarrete
     * @return void
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
