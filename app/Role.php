<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

        
    /**
     * users
     * Relación de uno a muchos entre roles y usarios
     * @author David Navarrete
     * @return void
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
