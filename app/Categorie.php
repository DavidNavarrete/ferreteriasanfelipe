<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'slug'];
    
    /**
     * getRouteKeyName
     * Retorna el slug de la categoría para mostrarlo en la url
     * @author David Navarrete
     * @return string slug
     */
    public function getRouteKeyName()
    {
        return 'slug';
    } 
    
    /**
     * subcategories
     * Relacion de uno a muchos entre categoría y subcategorias
     * @author David Navarrete
     * @return void
     */
    public function subcategories(){
        return $this->hasMany('App\Subcategorie');
    }
}
