<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * role
     * Relación de uno a muchos entre usuarios y roles
     * @author David Navarrete
     * @return void
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    
    /**
     * getFullName
     * Retorna el nombre y el apellido del usuario
     * @author David Navarrete
     * @return void
     */
    public function getFullName(){
        return $this->name.' '.$this->lastname;
    }
        
    /**
     * getEmail
     * Retorna el correo del usuario
     *  @author David Navarrete
     * @return void
     */
    public function getEmail(){
        return $this->email;
    }
        
    /**
     * isAdmin
     * Valida si un usuario es admin o no
     *  @author David Navarrete
     * @return void
     */
    public function isAdmin(){
        if($this->role->name == "Admin"){
            return 1;
        }else{
            return 0;
        }
    }
   
   /**
    * sendPasswordResetNotification
    * Envía el enlace de recuperación de contraseña
    * @param  string $token
    * @return void
    */
   public function sendPasswordResetNotification($token)
   {
    $this->notify(new ResetPasswordNotification($token));
   }
}
