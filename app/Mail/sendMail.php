<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    public $user;
    public $total;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details, $user, $total)
    {
        $this->details = $details;
        $this->user = $user;
        $this->total = $total;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Mi compra en Ferreteria San Felipe')->view('sendMail')->with(['details', $this->details, 'user' => $this->user, 'total', $this->total]);
    }
}
