<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['name', 'slug', 'image', 'enabled'];   
    /**
     * getRouteKeyName
     * Retorna el slug a la url
     * @author David Navarrete
     * @return string slug
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
        
    /**
     * products
     * Relación de uno a muchos entre temporada y productos
     * @author David Navarrete
     * @return void
     */
    public function products(){
        return $this->hasMany('App\Product');
     }
}
