@extends('layouts.plantillaHome')

@section('content')
<div class="row justify-content-center">
 @include('common.errors')
  <div class="col-12 col-lg-12 pb-5">
              <form id="form" class="form-group" method="POST" action="/home/subcategorias" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de subcategorías</h3>
                              </div>
                          </div>
                          <div class="card-body p-0 my-3">
                              <div class="form-group col-12 col-lg-11 mx-auto">
                              <label class="text-dark font-weight-bold d-block d-sm-none" for="Nombre">Nombre de la subcategoría</label>
                                  <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                          <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                      </div>
                                    <input aria-label="Nombre" id="validationName" type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Nombre de la subcategoría" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                                  </div>
                                  <label class="text-dark font-weight-bold d-block d-sm-none" for="NombreCategoria">Seleccione la categoría</label>
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                    </div>
                                    <select aria-label="Categoría" id="validationCategorie" class="custom-select" name="categorie_id" required>
                                      <option disabled selected value="">-- Seleccione una categoría --</option>
                                      @if (isset($categories) && $categories ?? '')
                                        @foreach ($categories as $c)
                                            <option  value="{{$c->id}}" {{old('categorie_id') == $c->id ? 'selected': ''}}>{{$c->name}}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                </div>
                                <div class="mx-auto my-4">
                                    <a href="{{route('subcategorias')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                 </div>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
        </div>
@endsection

