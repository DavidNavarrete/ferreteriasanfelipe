@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
      <div class="col-12 col-lg-12 pb-5">
          @if (isset($subcategorie) && $subcategorie ?? '') 
            <form id="form" class="form-group" method="POST" action="/home/subcategorias/{{$subcategorie->slug}}" enctype="multipart/form-data">
                    @method('PUT')
                        @csrf
                                <div class="card shadow-lg">
                                    <div class="card-header bg-primary p-0">
                                        <div class="text-white text-center py-2">
                                            <h3 class="font-weight-bold"> Actualización de subcategorías</h3>
                                        </div>
                                    </div>
                                    <div class="card-body p-0 my-2">
                                        <div class="form-group col-12 col-lg-11 mx-auto">
                                            <label class="d-block text-dark font-weight-bold" for="name">Nombre de subcategoría</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                                </div>
                                                <input aria-label="Nombre" id="validationName" type="text" name="name" value="{{$subcategorie->name}}" class="form-control" placeholder="Ingrese el nombre" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                                            </div>
                                            <label class="d-block text-dark font-weight-bold" for="categorie_id">Categoría</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                            </div>
                                            <select aria-label="Categoría" class="custom-select" name="categorie_id" required>
                                                @if (isset($categories) && $categories ?? '')
                                                    @foreach ($categories as $c)
                                                        <option  value="{{$c->id}}" @if(isset($subcategorie->categorie_id) && $c->id == $subcategorie->categorie_id) selected @endif>
                                                            {{$c->name}}
                                                        </option>
                                                    @endforeach 
                                                @endif
                                            </select>
                                            </div>
                                            <div class="mx-auto my-4">
                                            <a href="{{route('subcategorias')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                            <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                        </div>  
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                  </div>
            </div>    
@endsection

