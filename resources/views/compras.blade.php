@extends('layouts.plantilla')
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')
    
<div class="container-fluid p-1 my-2">
        <div class="card-header bg-primary text-white">
                <div class="row">
                    <div class="col text-center">
                        <h6 class="font-weight-bold d-inline text-uppercase">Mis compras</h6>  
                    </div>
                </div>
        </div>
</div>
@if(isset($buys) && $buys ?? '')
<div class="container-fluid">
  <div class="row">
  @foreach($buys as $buy)
    <div class="col-12 col-md-6">
        <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-md-4 text-center">
          <i class="fas fa-shopping-bag fa-4x my-5 text-primary"></i>
        </div>
        <div class="col-12 col-md-8">
          <div class="card-body">
            <h5 class="card-title text-uppercase">Mi compra - N° Orden: {{$buy->order}}</h5>
            @if($buy->status == 'Lista para retiro')
            <h6 class="card-text text-uppercase">
              Estado: 
              <span class="badge badge-pill badge-info">{{$buy->status}}</span>
            </h6>
            @else
              @if($buy->status == 'Aceptada')
                  <h6 class="card-text text-uppercase">
                  Estado: 
                  <span class="badge badge-pill badge-success">{{$buy->status}}</span>
                </h6>
                @else
                  <h6 class="card-text text-uppercase">
                Estado: 
                <span class="badge badge-pill badge-danger">{{$buy->status}}</span>
              </h6>
              @endif
            @endif
            <p class="card-text">Aquí puedes ver la información detallada de la compra que realizaste.</p>
            <p class="card-text"><small class="text-muted">{{$buy->created_at}}</small></p>
            <a href="{{route('detalle', $buy->order)}}" class="btn btn-outline-primary">Ver detalle  <i class="fa fa-info-circle"></i> </a>
          </div>
        </div>
      </div>
    </div>
    </div>
    @endforeach
  </div>
</div>
@else
<div class="container-fluid">
  <div class="row justify-content-center">
  <div class="col-12 col-md-6">
        <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-md-4 text-center">
          <i class="fas fa-shopping-bag fa-4x my-5 text-primary"></i>
        </div>
        <div class="col-12 col-md-8">
          <div class="card-body">
            <h5 class="card-title text-uppercase">No se hn realizado compras</h5>
            <p class="card-text">Realiza una compra para ver información de ellas.</p>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
@endif

@endsection