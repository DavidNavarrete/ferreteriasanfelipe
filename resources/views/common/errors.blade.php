 @if ($errors->any())
    @foreach ($errors->all() as $e)
    <ul class="list-unstyled">
      <li class="alert alert-danger">
        <div class="text-dark-100 font-weight-bold">
          <i class="fa fa-times mr-2"></i>{{$e}}
        </div>
        </li>
      </ul>
    @endforeach
    @endif