@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
                <form id="form" class="form-group"  method="POST" action="/home/temporadas" enctype="multipart/form-data" >
                    @csrf
                      <div class="card shadow-lg">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de temporadas</h3>
                              </div>
                          </div>
                          <div class="card-body p-0 my-3">
                            <div class="form-group col-12 col-lg-10 mx-auto">
                              <label class="text-dark font-weight-bold d-block d-sm-none" for="Nombre">Nombre de temporada</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i class="fa fa-pen text-white"></i></div>
                                    </div>
                                <input aria-label="Nombre" id="validationName" type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Nombre de la temporada" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                                </div>
                                <label class="d-block text-dark font-weight-bold" for="Imagen">Imagen</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="custom-file uploadfrm">
                                            <input type="file" accept="image/*" name="image" class="custom-file-input" onchange="readURL(this);" required>
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 text-center">
                                        <div class="preview">
                                            <img id="img-preview" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Vista previa de su imagen</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block d-sm-none text-dark font-weight-bold" for="Habilitado">Habilitar</label>
                              <div class="input-group mb-2 my-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text bg-gradient-primary"> <i class="fa fa-star text-white"></i> </div>
                                </div>
                                <select aria-label="Habilitado" id="validationDestacated" class="custom-select" name="enabled" required>
                                  <option disabled selected value="">-- ¿Habilitar para mostrar en vista principal? --</option>
                                      <option value="si" {{old('enabled') == "si" ? 'selected' : ''}}>Sí</option>
                                      <option value="no" {{old('enabled') == "no" ? 'selected' : ''}}>No</option>
                                </select>
                            </div>
                            <div class="mx-auto my-4">
                                <a href="{{route('temporadas')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                              </div>
                            </div>
                          </div>
                      </div>
                  </form>
              </div>
        </div>
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection

