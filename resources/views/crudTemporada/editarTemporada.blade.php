@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
      <div class="col-12 col-lg-12 pb-5">
        @if (isset($season) && $season ?? '')
          <form id="form" class="form-group" method="POST" action="/home/temporadas/{{$season->slug}}" enctype="multipart/form-data">
                  @method('PUT')
                      @csrf
                            <div class="card shadow-lg">
                                <div class="card-header bg-primary p-0">
                                    <div class="text-white text-center py-2">
                                        <h3 class="font-weight-bold"> Actualización de temporadas</h3>
                                    </div>
                                </div>
                                <div class="card-body p-0 my-3">
                                    <div class="form-group col-12 col-lg-10 mx-auto">
                                        <label class="d-block text-dark font-weight-bold my-1" for="name">Nombre de temporada</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                            </div>
                                            <input aria-label="Nombre" id="validationName" type="text" name="name" value="{{$season->name}}" class="form-control" placeholder="Ingrese el nombre" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                                        </div>
                                        <label class="d-block text-dark font-weight-bold" for="Imagen">Imagen</label>
                                        <div class="row">
                                                    <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="custom-file uploadfrm">
                                                            <input type="file" accept="image/*" name="image" class="custom-file-input" onchange="readURL(this);" required>
                                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                                        </div>
                                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-lg-6 text-center col-xl-4">
                                                        <div class="preview">
                                                            <img id="img-actual" src="{{$season->image}}" alt="Vista previa de imagen actual" height="150" width="150" class="img-thumbnail border-0">
                                                            <small class="d-block">Imagen actual de temporada</small>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-lg-12 text-center col-xl-4">
                                                    <div class="preview">
                                                        <img id="img-preview" src="https://placehold.it/150x150" alt="Vista previa de imagen nueva" height="150" width="150" class="img-thumbnail border-0">
                                                        <small class="d-block">Nueva imagen de temporada</small>
                                                    </div>
                                                    </div>
                                                </div>
                                        <label class="d-block text-dark font-weight-bold my-1">Habilitado</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"> <i class="fa fa-star text-white"></i> </div>
                                            </div>
                                            <select aria-label="Habilitado" class="custom-select" name="enabled">
                                                <option disabled>-- ¿Habilitar para mostrar en vista principal? --</option>
                                                @if($season->enabled == "si")
                                                    <option value="si" selected>Sí</option>
                                                    <option value="no">No</option>
                                                @else
                                                    <option value="si">Sí</option>
                                                    <option value="no" selected>No</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="mx-auto my-4">
                                          <a href="{{route('temporadas')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                          <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                        </div>   
                                    </div>       
                              </div>
                            </div>
                        </form>
                    @endif
                </div>
        </div>
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection

