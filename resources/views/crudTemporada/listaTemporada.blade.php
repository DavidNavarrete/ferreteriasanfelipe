@extends('layouts.plantillaHome')

@section('content')
          <div class="card shadow-lg mb-4">
            <div class="card-header bg-primary py-3">
              <h5 class="m-0 font-weight-bold text-white">Panel de administración de temporadas</h5>
            </div>
          <div class="card-body">
              <div class="container-fluid">  
                <!-- Barra de busqueda -->
                <form class="col-sm-8 col-md-8 col-lg-6 d-none d-sm-inline-block p-0">
                  <div class="input-group">
                    <input type="text" name="searchseason" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
                    <div class="input-group-append">
                      <button aria-label="Buscador" class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
          <ul class="navbar-nav ml-auto">
            <!-- Boton para abrir la barra de busqueda -->
            <li class="nav-item dropdown no-arrow d-sm-none text-right">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw fa-2x text-primary"></i>
              </a>
              <!-- Fin Boton para abrir la barra de busqueda -->
              <!-- Barra de busqueda en pantallas pequeñas -->
              <div class="dropdown-menu dropdown-menu p-3 shadow" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" name="searchseason" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
                    <div class="input-group-append">
                      <button aria-label="Buscador" class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
                <!-- Fin Barra de busqueda en pantallas pequeñas -->
            </li>
          </ul>
              <!-- Resultados de búsqueda-->
              <div class="my-2">
                @if(isset($query) && $query ?? '') 
                  <div class="alert alert-success">
                    <h6 class="d-inline">
                      @if (isset($seasons) && $seasons->count() > 0)
                        Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                      @else
                        No existen resultados para: <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> 
                      @endif
                    </h6>
                  </div>
                   <div class="d-flex justify-content-start">
                    <a href="{{route('temporadas')}}" type="submit" role="button" class="btn btn-outline-primary">Mostrar todas</a>
                  </div>
                @endif
             </div>
              <!-- Fin Resultados de búsqueda-->
              @include('common.success')
                <div class="text-right my-1 p-0">
                  <a href="{{route('temporadas.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
                </div>
                  <div class="row">
                    @if (isset($seasons) && $seasons ?? '')
                        @foreach ($seasons as $season)
                          <div class="col-12 col-sm-6 col-lg-3 text-dark">
                          <div class="card shadow my-1 p-0">
                          <!-- /images/temporadas/{{$season->image}} -->
                          <img class="card-img-top" src="{{$season->image}}" width="300" height="200" alt="{{'Imagen de temporada '.$season->image}}" title="{{'Imagen de temporada '.$season->image}}">
                            <div class="card-body">
                            <h5 title="{{$season->name}}" class="card-title text-uppercase font-weight-bold">{{Str::limit($season->name, $limit="12", $end="...")}}</h5>
                            @if (isset($season->enabled) && $season->enabled == "si")
                              <h6 class="card-title badge badge-success py-2 text-uppercase card-link">Habilitada <i class="fa fa-star text-white"></i> </h6>
                              @else
                              <h6 class="card-title badge badge-light py-2 text-uppercase">No habilitada</h6>
                              @endif
                              <div class="text-left">

                                <a aria-label="Editar" href="{{route('temporadas.edit', $season->slug)}}" role="button" type="button" class="btn btn-info text-white"><span class="fa fa-edit"></span></a>    
                                <a aria-label="Eliminar" data-toggle="modal" data-target="#openModal{{$season->id}}" role="button" type="button" class="btn btn-danger"> <span class="fa fa-trash text-white"></span></a>
                              </div>
                              <!-- Modal -->
                              <div class="modal fade" id="openModal{{$season->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header bg-primary text-white">
                                      <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Eliminar subcategoría</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span class="text-white active" aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body font-weight-bold">
                                        <div class="text-center">
                                          <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                                        </div>
                                        ¿Está seguro de eliminar esta temporada: "{{$season->name}}" ?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                        <a href="{{route('temporada.borrar', $season->slug)}}" type="button" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span> </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    @endif
                  </div>
                  <div class="d-flex justify-content-center my-2">
                    @if (isset($seasons) && $seasons ?? '')
                      {{$seasons->links()}}
                    @endif
                  </div>
                </div>
              </div>
          </div>      
@endsection


