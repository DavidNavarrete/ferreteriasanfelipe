@extends('layouts.plantillaErrors')
@section('title')
    500 - Error interno
@endsection
@section('content')
<div class="container text-center">
        <h3 class="font-weight-bold mt-5 text-white">ERROR 500 - INTERNAL SERVER ERROR</h3>
        <img src="/images/errors/500.png" alt="Error 419" class="img-fluid my-1 py-3" height="300" width="300">
        <h4 class="text-white">Surgió un problema, lamentamos las molestias...</h4>
        <a href="{{route('principal')}}" type="button" class="btn btn-light btn-lg my-3">Volver al inicio <i class="fa fa-home"></i> </a>
</div>
@endsection