<div class="modal fade" id="modal-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <div class="modal-title font-weight-bold">Descripción del producto</div>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                        <div class="container-fluid p-0">
                            <div class="row">
                                <div class="preview col-12 col-sm-10 mx-auto col-lg-6 col-xl-6 mx-auto rounded shadow my-1">
                                    <div class="preview-pic tab-content my-auto">
                                        
                                    </div>
                                    <ul class="preview-thumbnail nav nav-tabs my-auto">
                                    
                                    </ul>
                                </div>
                                <div class="col-12 text-center col-sm-9 mx-auto col-lg-6 col-xl-6 mx-auto my-4">
                                        <h3 class="text-uppercase font-weight-bold product-title">Nombre del producto </h3>
                                            <p class="text-lowercase product-description">Descripción del producto.</p>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 class="font-weight-bold product-price">$ Precio del producto</h3>
                                                <h5 class="d-inline">Cantidad disponible: <h5 class="font-weight-bold product-stock d-inline">Stock del producto</h5></h5>
                                            </div>
                                        </div>
                                        <div class="my-3">
                                            <a href="" id="link" class="btn btn-primary btn-lg text-white text-uppercase font-weight-bold shadow" type="button">Agregar al carro
                                            </a>
                                        </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>