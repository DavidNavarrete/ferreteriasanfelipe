@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
          @if (isset($images) && $images ?? '')
          <form id="form" class="form-group" method="POST" action="/home/imagenes/{{$images->id}}" enctype="multipart/form-data">
              @method('PUT')       
                      @csrf
                        <div class="card shadow-lg p-0">
                            <div class="card-header p-0">
                                <div class="bg-primary text-white text-center py-2">
                                    <h3 class="font-weight-bold"> Actualización de imagenes del producto</h3>
                                </div>
                            </div>
                            <div class="card-body p-0 text-dark">
                              <div class="form-group col-12 col-lg-10 mx-auto">
                              <label class="d-block text-dark font-weight-bold my-1" for="Imagen">Imagen</label>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-4">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Imagen1" type="file" accept="image/*" name="image1" class="custom-file-input" onchange="readURL(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center col-xl-4">
                                           <div class="preview">
                                                            <img id="img-actual" src="{{$images->image1}}" alt="Vista previa de imagen actual" height="150" width="150" class="img-thumbnail border-0">
                                                            <small class="d-block">Imagen actual de temporada</small>
                                                        </div>
                                                </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center">
                                        <div class="preview">
                                            <img id="img-preview1" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Imagen nueva</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block text-dark font-weight-bold" for="Imagen2">Imagen opcional 2</label>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-4">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Imagen2" type="file" accept="image/*" name="image2" class="custom-file-input" onchange="readURLImg2(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center col-xl-4">
                                           <div class="preview">
                                                            <img id="img-actual" src="{{$images->image2}}" alt="Vista previa de imagen actual" height="150" width="150" class="img-thumbnail border-0">
                                                            <small class="d-block">Imagen actual de temporada</small>
                                                        </div>
                                                </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center">
                                        <div class="preview">
                                            <img id="img-preview2" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Imagen nueva (2)</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block text-dark font-weight-bold" for="Imagen">Imagen opcional 3</label>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-xl-4">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Image3" type="file" accept="image/*" name="image3" class="custom-file-input" onchange="readURLImg3(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center col-xl-4">
                                           <div class="preview">
                                                            <img id="img-actual" src="{{$images->image3}}" alt="Vista previa de imagen actual" height="150" width="150" class="img-thumbnail border-0">
                                                            <small class="d-block">Imagen actual de temporada</small>
                                                        </div>
                                                </div>
                                    <div class="col-12 col-md-6 col-lg-4 text-center">
                                        <div class="preview">
                                            <img id="img-preview3" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Imagen nueva (3)</small>
                                        </div>
                                    </div>
                                </div>
                                <label for="Producto" class="d-block text-dark font-weight-bold my-2">Selección de producto</label>
                                <div class="input-group mb-2 my-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text bg-gradient-primary"> <i class="fa fa-store text-white"></i> </div>
                                  </div>
                                  <select aria-label="Producto" class="custom-select" name="product_id" id="validationProduct" required>
                                    @if (isset($products) && $products ?? '')
                                      @foreach ($products as $p)
                                        <option  value="{{$p->id}}" @if(isset($images->product_id) && $images->product_id == $p->id) selected @endif>
                                            {{$p->name}}
                                        </option>
                                      @endforeach
                                    @endif
                                </select>
                              </div>
                              <div class="mx-auto my-4">
                                <a href="{{route('imagenes')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                  <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                              </div>
                            </div>   
                            </div>
                        </div>
                    </form>
                  @endif
              </div>
        </div>
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview1')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
function readURLImg2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview2')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
function readURLImg3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview3')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection

  