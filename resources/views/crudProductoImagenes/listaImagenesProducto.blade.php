@extends('layouts.plantillaHome')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-primary">
          <h5 class="m-0 font-weight-bold text-white">Panel de administración de productos con imagenes secundarias</h5>
        </div>
        <div class="card-body">
          <div class="container-fluid">
          <!-- Barra de busqueda -->
          <form  class="col-sm-8 col-md-8 col-lg-6 d-none d-sm-inline-block p-0">
            <div class="input-group">
              <input type="text" name="search" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Search">
              <div class="input-group-append">
                <button aria-label="Buscador" class="btn btn-primary" type="submit">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Boton para abrir la barra de busqueda -->
            <li class="nav-item dropdown no-arrow d-sm-none text-right">
              <a class="nav-link dropdown-toggle" aria-label="Buscador" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw fa-2x text-primary"></i>
              </a>
              <!-- Fin Boton para abrir la barra de busqueda -->
              <!-- Barra de busqueda en pantallas pequeñas -->
              <div class="dropdown-menu dropdown-menu p-3 shadow" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Search">
                    <div class="input-group-append">
                      <button aria-label="Buscador" class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- Fin Barra de busqueda en pantallas pequeñas -->
            </li>
          </ul>
           <!-- Resultados de búsqueda-->
              <div class="my-2">
                @if(isset($query) && $query ?? '') 
                  <div class="alert alert-success">
                    <h6 class="d-inline">
                      @if (isset($imagesproduct) && $imagesproduct->count() > 0)
                        Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                      @else
                        No existen resultados para: <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> 
                      @endif
                    </h6>
                  </div>
                  <div class="d-flex justify-content-start">
                    <a href="{{route('imagenes')}}" type="submit" role="button" class="btn btn-outline-primary">Mostrar todas</a>
                  </div>
                @endif
             </div>
              @include('common.success')
            <!-- Fin Resultados de búsqueda-->
              <div class="text-right my-1 p-0">
                <a href="{{route('imagenes.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
              </div>
              <div class="row">
                @if (isset($imagesproduct) && $imagesproduct ?? '')
                  @foreach ($imagesproduct as $products)
                   <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 my-2">
                     <div class="card shadow text-dark my-1 h-100">
                     <img  width="240" height="231" class="card-img-top" src="{{$products->product->image}}" title="{{'Imagen de '.$products->image}}" alt="{{'Imagen de '.$products->product->image}}">
                     <h5 title="{{$products->product->name}}" class="text-center card-title font-weight-bold text-uppercase my-2">{{Str::limit($products->product->name, $limit="14", $end="...")}}</h5>
                       <div class="card-body text-center bg-white">
                         <p class="text-uppercase">Imagenes secundarias</p>
                          @if ($products->image1 ?? '')
                            <img src="{{$products->image1}}" height="76" width="76" title="Imagen 1 de '{{$products->product->name}}" alt="Imagen 1 de '{{$products->product->name}}" class="img-fluid mr-1 my-1">   
                          @endif
                          @if ($products->image2 ?? '')
                            <img src="{{$products->image2}}" height="76" width="76" title="Imagen 2 de '{{$products->product->name}}" alt="Imagen 2 de '{{$products->product->name}}" class="img-fluid mr-1 my-1">   
                          @endif
                          @if ($products->image3 ?? '')
                            <img src="{{$products->image3}}" height="76" width="76" title="Imagen 3 de '{{$products->product->name}}" alt="Imagen 3 de '{{$products->product->name}}" class="img-fluid mr-1 my-1">   
                          @endif
                       </div>
                       <div class="card-footer text-center bg-white">
                         <a aria-label="Editar" href="{{route('imagenes.edit', $products->id)}}" role="button" type="button" class="btn btn-info text-white"><span class="fa fa-edit"></span></a> 
                           <a aria-label="Eliminar" data-toggle="modal" data-target="#openModal{{$products->id}}" role="button" type="button" class="btn btn-danger"> <span class="fa fa-trash text-white"></span></a>
                             <div class="modal fade" id="openModal{{$products->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                               <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                   <div class="modal-header bg-primary text-white">
                                     <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Eliminar imagenes secundarias</h5>
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span class="text-white active" aria-hidden="true">&times;</span>
                                     </button>
                                     </div>
                                     <div class="modal-body font-weight-bold">
                                       <div class="text-center">
                                         <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                                       </div>
                                       ¿Está seguro de eliminar las imagenes secundarias de este producto: "{{$products->product->name}}" ?
                                     </div>
                                     <div class="modal-footer">
                                       <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                       <a href="{{route('imagenes.borrar',$products->id)}}" type="button" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span> </a>
                                   </div>
                                 </div>
                               </div>
                             </div>
                         </div>
                       </div>
                     </div>
                     @endforeach
                @endif
              </div>
            <div class="d-flex justify-content-center my-2">
              @if (isset($imagesproduct) && $imagesproduct ?? '')
                {{$imagesproduct->links()}}     
              @endif
          </div>
       </div>
   </div>
</div>
@endsection