@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
  <div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
            <form id="form" class="form-group" method="POST" action="/home/imagenes" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg p-0">
                            <div class="card-header p-0">
                                <div class="bg-primary text-white text-center py-2">
                                    <h3 class="font-weight-bold"> Registro de imagenes del producto</h3>
                                </div>
                            </div>
                            <div class="card-body text-dark p-3">
                              <div class="form-group col-12 col-lg-10 mx-auto ">
                              <label class="d-block text-dark font-weight-bold" for="Imagen">Imagen</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Imagen1" type="file" accept="image/*" name="image1" class="custom-file-input" onchange="readURL(this);" required>
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 text-center">
                                        <div class="preview">
                                            <img id="img-preview1" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Vista previa de su imagen</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block text-dark font-weight-bold" for="Imagen2">Imagen opcional 2</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Imagen2" type="file" accept="image/*" name="image2" class="custom-file-input" onchange="readURLImg2(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 text-center">
                                        <div class="preview">
                                            <img id="img-preview2" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Vista previa de su imagen 2</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block text-dark font-weight-bold" for="Imagen">Imagen opcional 3</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="custom-file uploadfrm">
                                            <input aria-label="Image3" type="file" accept="image/*" name="image3" class="custom-file-input" onchange="readURLImg3(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 text-center">
                                        <div class="preview">
                                            <img id="img-preview3" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Vista previa de su imagen 3</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-block d-sm-none text-dark font-weight-bold my-2" for="Producto">Seleccione el producto</label>       
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text bg-gradient-primary"> <i class="fa fa-store text-white"></i> </div>
                                  </div>
                                    <select aria-label="Producto" class="custom-select" id="validationProduct" name="product_id" >
                                    <option selected disabled value="">-- Seleccione un producto --</option>
                                    @if (isset($products) && $products ?? '') 
                                      @foreach ($products as $p)
                                        <option  value="{{$p->id}}" {{old('product_id') == $p->id ? 'selected' : ''}}>{{$p->name}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                              <div class="mx-auto my-4">
                                <a href="{{route('imagenes')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                              </div>
                            </div>
                      </div>
                    </div>
                </form>
           </div>
     </div>
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview1')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
function readURLImg2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview2')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
function readURLImg3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview3')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection


