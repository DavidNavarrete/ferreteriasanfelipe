@extends('layouts.plantillaHome')
@section('content')
        <div class="card shadow-lg mb-4">
            <div class="card-header bg-primary text-white py-3">
              <h4 class="m-0 font-weight-bold">Panel de administración de información empresarial</h4>
            </div>
            <div class="card-body">
              <div class="container-fluid">
              @include('common.success')
              @if (isset($information) && $information->count() == 0)
                <div class="text-right my-1 p-0">
                  <a href="{{route('informacion.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
                </div>
              @else
                <div class="text-right my-1 p-0">
                <a role="button" type="button" class="btn btn-primary text-white disabled" aria-disabled="true">Agregar <span class="fa fa-plus-circle"></span></a>
                </div>
              @endif
              <div class="row">
                @if (isset($information) && $information ?? '')
                  @foreach ($information as $info)
                  <div class="col-lg-5">
                    <div class="card">
                        <div class="card-header font-weight-bold bg-primary text-white">Horarios</div>
                        <div class="card-body text-dark">
                            <ul class="list-unstyled">
                            <li><a class="font-weight-bold"><i class="fas fa-clock fa-2x mr-2 text-primary"></i>Horario normal: {{$info->normal_working_hours}}</a></li>
                            <li><a class="font-weight-bold"><i class="fas fa-clock fa-2x mr-2 text-primary"></i>Horario Domingo: {{$info->working_hours_sunday}}</a></li>
                            <li><a class="font-weight-bold"><i class="fas fa-clock fa-2x mr-2 text-primary"></i>Horario Festivo: {{$info->holidays_working_hours}}</a></li>
                            </ul>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-7">
                    <div class="card mb-3 my-1">
                        <div class="card-header font-weight-bold bg-primary text-white">Nuestros valores</div>
                        <div class="card-body text-dark">
                            <div class="row">
                            <div class="col-lg-6 my-2">
                                <div class="card text-center">
                                    <a>
                                        <i class="fa fa-heart fa-4x text-primary my-1"></i>
                                        <h3 class="m-b-xs"><strong>Misión</strong></h3>
                                    </a>
                                    <div class="card-footer bg-gradient-primary text-white">
                                        {{$info->mission}}
                                    </div>    
                                </div>
                            </div>
                            <div class="col-lg-6 my-2">
                                <div class="card text-center">
                                    <a>
                                        <i class="fa fa-heart fa-4x text-primary my-1"></i>
                                        <h3 class="m-b-xs"><strong>Visión</strong></h3>
                                    </a>
                                    <div class="card-footer bg-gradient-primary text-white">
                                        {{$info->vision}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-5">
                    <div class="card border mb-3">
                        <div class="card-header font-weight-bold bg-primary text-white">Sobre nosotros</div>
                        <div class="card-body text-dark">
                          <div class="card-text">{{$info->description}}</div>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-7">
                    <div class="card border mb-3">
                        <div class="card-header font-weight-bold bg-primary text-white">Nuestra ubicación</div>
                        <div class="card-body text-dark">
                            <div class="card-text">
                                {{$info->ubication}}
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12 mx-auto">
                    <div class="d-flex justify-content-end">
                      <a href="{{route('informacion.edit', $info->id)}}" role="button" type="button" class="btn btn-info text-white">Editar <span class="fa fa-edit"></span></a>
                    </div>
                  </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>   
@endsection


