@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
            <form class="form-group" method="POST" action="/home/informacion" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de información empresarial</h3>
                              </div>
                          </div>
                          <div class="card-body p-3">
                            <div class="card mb-3">
                                <div class="card-header font-weight-bold bg-primary text-white">Horarios</div>
                                <div class="card-body text-dark">
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label  class="d-block" for="normal_working_hours_start">Horario normal (Inicio)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario normal" type="time" id="validationStart1" name="normal_working_hours_start" value="{{old('normal_working_hours_start')}}" class="form-control clockpicker" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label  class="d-block" for="normal_working_hours_end">Horario normal (Fin)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario normal" type="time" id="validationEnd1" name="normal_working_hours_end" class="form-control clockpicker" value="{{old('normal_working_hours_end')}}" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label class="d-block" for="working_hours_sunday_start">Horario Domingo (Inicio)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario Domingo" type="time" id="validationStart2" name="working_hours_sunday_start" class="form-control clockpicker" value="{{old('working_hours_sunday_start')}}" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label class="d-block" for="working_hours_sunday_end">Horario Domingo (Fin)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario Domingo" type="time" id="validationEnd2" name="working_hours_sunday_end" class="form-control clockpicker" value="{{old('working_hours_sunday_end')}}" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label class="d-block" for="holidays_working_hours_start">Horario Festivos (Inicio)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario festivos" type="time" id="validationStart3" name="holidays_working_hours_start" class="form-control clockpicker" value="{{old('holidays_working_hours_start')}}" readonly required>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                        <label class="d-block" for="holidays_working_hours_end">Horario Festivos (Fin)</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                            </div>
                                        <input aria-label="Horario festivos" type="time" id="validationEnd3" name="holidays_working_hours_end" class="form-control clockpicker" value="{{old('holidays_working_hours_end')}}" readonly required>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            <div class="card mb-3">
                                <div class="card-header font-weight-bold bg-primary text-white">Nuestros valores</div>
                                <div class="card-body text-dark">
                                    <div class="form-group col-12 col-lg-11 mx-auto">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-heart text-white"></i></div>
                                            </div>
                                        <textarea aria-label="Misión" type="text" id="validationMission" name="mission" class="form-control" placeholder="Ingrese la misión" required>{{old('mission')}}</textarea>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-heart text-white"></i></div>
                                            </div>
                                        <textarea aria-label="Visión" type="text" id="validationVision" name="vision" class="form-control" placeholder="Ingrese la visión" required>{{old('vision')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            <div class="card border mb-3">
                                <div class="card-header font-weight-bold bg-primary text-white">Descripción de nuestra empresa</div>
                                <div class="card-body text-dark">
                                    <div class="form-group col-12 col-lg-11 mx-auto">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-pencil-alt text-white"></i></div>
                                            </div>
                                        <textarea aria-label="Descripción" type="text" id="validationDescription" name="description" class="form-control" placeholder="Ingrese la descripción de su empresa" required>{{old('description')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            <div class="card border mb-3">
                                <div class="card-header font-weight-bold bg-primary text-white">Nuestra ubicación</div>
                                <div class="card-body text-dark">
                                    <div class="form-group col-12 col-lg-11 mx-auto">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-map-marker-alt text-white"></i></div>
                                            </div>
                                        <input aria-label="Ubiación" type="text" id="validationUbication" name="ubication" class="form-control" value="{{old('ubication')}}" placeholder="Ingrese la ubicación" required>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="mx-auto my-4">
                                <a href="{{route('informacion')}}" class="btn btn-light"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                <button id="btnSubmit" class="btn btn-primary ml-2">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
        </div>
@endsection
@section('scripts')
    <script src="{{asset('js/clockpicker.js')}}"></script>
    <script src="{{asset('js/openClockPicker.js')}}"></script>
@endsection


