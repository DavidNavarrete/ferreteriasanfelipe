@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
          @if (isset($information) && $information ?? '')
            <form id="form" class="form-group" method="POST" action="/home/informacion/{{$information->id}}" enctype="multipart/form-data">
                @method('PUT')
                        @csrf
                          <div class="card shadow-lg">
                              <div class="card-header bg-primary p-0">
                                  <div class="text-white text-center py-2">
                                      <h3 class="font-weight-bold"> Actualización de información empresarial</h3>
                                  </div>
                              </div>
                              <div class="card-body p-3">
                                <div class="card border mb-3">
                                    <div class="card-header font-weight-bold bg-primary text-white">Horarios</div>
                                    <div class="card-body text-dark">
                                            
                                  <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="normal_working_hours_start">Horario normal (Inicio)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                    <input aria-label="Horario normal" type="time" id="validationStart1" @if(isset($normal_working_hours_start) && $normal_working_hours_start ?? '') value="{{$normal_working_hours_start}}"@endif name="normal_working_hours_start" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="normal_working_hours_end">Horario normal (Fin)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                        <input aria-label="Horario normal" type="time" id="validationEnd1" @if(isset($normal_working_hours_end) && $normal_working_hours_end) value="{{$normal_working_hours_end}}"@endif name="normal_working_hours_end" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="working_hours_sunday_start">Horario Domingo (Inicio)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                    <input aria-label="Horario Domingo" type="time" id="validationStart2" @if(isset($working_hours_sunday_start) && $working_hours_sunday_start) value="{{$working_hours_sunday_start}}"@endif name="working_hours_sunday_start" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="working_hours_sunday_end">Horario Domingo (Fin)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                        <input aria-label="Horario Domingo" type="time" id="validationEnd2" @if(isset($working_hours_sunday_end) && $working_hours_sunday_end ?? '') value="{{$working_hours_sunday_end}}@endif" name="working_hours_sunday_end" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="holidays_working_hours_start">Horario Festivos (Inicio)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                    <input aria-label="Horario festivos" type="time" id="validationStart3" @if(isset($holidays_working_hours_start) && $holidays_working_hours_start ?? '') value="{{$holidays_working_hours_start}}"@endif name="holidays_working_hours_start" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-lg-5 mx-auto d-inline-block">
                                    <label class="d-block" for="holidays_working_hours_end">Horario Festivos (Fin)</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-clock text-white"></i></div>
                                        </div>
                                        <input aria-label="Horario festivos" type="time" id="validationEnd3" @if(isset($holidays_working_hours_end) && $holidays_working_hours_end ?? '') value="{{$holidays_working_hours_end}}"@endif name="holidays_working_hours_end" class="form-control clockpicker" readonly required>
                                    </div>
                                </div>
                                    </div>
                                  </div>
                                <div class="card border mb-3">
                                    <div class="card-header font-weight-bold bg-primary text-white">Nuestros valores</div>
                                    <div class="card-body text-dark">
                                        <div class="form-group col-12 col-lg-11 mx-auto">
                                            <label for="mission">Misión</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-heart text-white"></i></div>
                                                </div>
                                            <textarea aria-label="Misión" id="validationMission" type="text" name="mission" class="form-control" placeholder="Ingrese la misión" required>{{$information->mission}}</textarea>
                                            </div>
                                            <label for="vision">Visión</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-heart text-white"></i></div>
                                                </div>
                                            <textarea aria-label="Visión" id="validationVision" type="text" name="vision" class="form-control" placeholder="Ingrese la visión" required>{{$information->vision}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                <div class="card border mb-3">
                                    <div class="card-header font-weight-bold bg-primary text-white">Descripción de nuestra empresa</div>
                                    <div class="card-body text-dark">
                                        <div class="form-group col-12 col-lg-11 mx-auto">
                                            <label for="description">Descripción</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-pencil-alt text-white"></i></div>
                                                </div>
                                            <textarea aria-label="Descripción" id="validationDescription" type="text" name="description" class="form-control" placeholder="Ingrese la descripción de su empresa" required>{{$information->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                <div class="card border mb-3">
                                    <div class="card-header font-weight-bold bg-primary text-white">Nuestra ubicación</div>
                                    <div class="card-body text-dark">
                                        <div class="form-group col-12 col-lg-11 mx-auto">
                                            <label for="ubication">Ubicación</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-map-marker-alt text-white"></i></div>
                                                </div>
                                            <input aria-label="Ubicación" id="validationUbication" type="text" value="{{$information->ubication}}" name="ubication" class="form-control" placeholder="Ingrese la ubicación" >
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="mx-auto my-4">
                                    <a href="{{route('informacion')}}" class="btn btn-light"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 my-1">Actualizar <i id="icon" data-original="fa fa-cloud-upload-alt ml-2" class="fa fa-cloud-upload-alt ml-2"></i></button>
                                  </div>
                              </div>
                          </div>
                      </form>
                    @endif
              </div>
        </div>
@endsection
@section('scripts')
    <script src="{{asset('js/clockpicker.js')}}"></script>
    <script src="{{asset('js/openClockPicker.js')}}"></script>
@endsection

