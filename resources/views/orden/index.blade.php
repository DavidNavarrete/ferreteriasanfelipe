@extends('layouts.plantillaHome')
@section('content')
    <div class="card">
        <div class="card-body text-center text-black">
            <!-- Barra de busqueda -->
        <div class="card-title text-uppercase font-weight-bold">Ingresa la orden de compra a buscar</div>
          <form  class="col-sm-8 col-md-8 col-lg-6 d-none d-sm-inline-block p-3">
            <div class="input-group">
              <input type="text" name="searchorder" class="form-control border small text-dark" placeholder="Ingresa tu orden de compra" aria-label="Search">
              <div class="input-group-append">
                <button aria-label="Buscador" class="btn btn-primary" type="submit">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
    </div>
    <!-- Resultados de búsqueda-->
    <div class="my-2">
                @if(isset($query) && $query ?? '') 
                  <div class="alert alert-success">
                    <h6 class="d-inline">
                      @if (isset($buys) && $buys->count() > 0)
                        Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                      @else
                        No existen resultados para: <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> 
                      @endif
                    </h6>
                  </div>
                @endif
             </div>
             @if(isset($buys) && $buys ?? '')
<div class="container-fluid">
  <div class="row">
  @foreach($buys as $buy)
    <div class="col-12 col-md-6">
        <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-md-4 text-center">
          <i class="fas fa-shopping-bag fa-4x my-5 text-primary"></i>
        </div>
        <div class="col-12 col-md-8">
          <div class="card-body">
            <h5 class="card-title text-uppercase font-weight-bold">Mi compra - N° Orden: {{$buy->order}}</h5>
            @if($buy->status == 'Lista para retiro')
            <h6 class="card-text text-uppercase">
              Estado: 
              <span class="badge badge-pill badge-info">{{$buy->status}}</span>
            </h6>
            @else
              @if($buy->status == 'Aceptada')
                  <h6 class="card-text text-uppercase">
                  Estado: 
                  <span class="badge badge-pill badge-success">{{$buy->status}}</span>
                </h6>
                @else
                  <h6 class="card-text text-uppercase">
                Estado: 
                <span class="badge badge-pill badge-danger">{{$buy->status}}</span>
              </h6>
              @endif
            @endif
            <p class="card-text">Aquí puedes ver la información detallada de la compra que realizaste.</p>
            <p class="card-text"><small class="text-muted">{{$buy->created_at}}</small></p>
            <form id="form" class="form-group" method="POST" action="/home/orden/{{$buy->id}}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <select aria-label="orden" class="custom-select" name="order" required>
                            <option disabled selected value="">-- Seleccione el estado de la orden --</option>
                                @if($buy->status == 'Lista para retiro')
                                  <option value="Lista para retiro" selected>Lista para retiro</option>
                                  <option value="Aceptada">Aceptada</option>
                                  <option value="Rechazada">Rechazada</option>
                                @else
                                  @if($buy->status == 'Aceptada')
                                  <option value="Lista para retiro">Lista para retiro</option>
                                  <option value="Aceptada" selected>Aceptada</option>
                                  <option value="Rechazada">Rechazada</option>
                                    @else
                                  <option value="Lista para retiro">Lista para retiro</option>
                                  <option value="Aceptada">Aceptada</option>
                                  <option value="Rechazada" selected>Rechazada</option>
                                  @endif
                                @endif
                              </select>
            </select>
          </div>
        </div>
      </div>
     <button class="btn btn-primary">Modificar estado <i class="fa fa-edit mr-1"></i></button>
    </form>
    </div>
    </div>
    @endforeach
  </div>
</div>
@endif
@endsection