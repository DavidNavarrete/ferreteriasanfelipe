
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="images/logotipo.ico">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Productos de la Ferretería San Felipe" />
        <meta name="author" content="Ferretería San Felipe" />
        <meta name="keywords" content="Ferretería, Productos, San Felipe, San Ignacio">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        {{-- Icons --}}
        <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="bg-primary">
   @yield('content')
</body>
</html>