<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" href="images/logotipo.ico">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Productos de la Ferretería San Felipe" />
        <meta name="author" content="Ferretería San Felipe" />
        <meta name="keywords" content="Ferretería, Productos, San Felipe, San Ignacio">
        <title>Ferretería San Felipe</title>
        <link rel="preload" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" as="style" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></noscript>
        <!-- Google fonts-->
        <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"></noscript>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="preload" as="style" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif&display=swap" rel="preload" as="style" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="preload" as="style" type="text/css" />
        <link rel="preload" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css"></noscript>
        <link rel="preload" href="{{asset('css/style.min.css')}}" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="{{asset('css/style.min.css')}}"></noscript>
        @yield('style')
    </head>
    <body>  
        @if ($token ?? '' && Request::path() == 'password/reset/'.$token)
        <style> 
            #mainNav{display:none;}
            #mainNav#logo{visibility: hidden;}
            footer{
                display: none;}
            body{background: rgb(19, 161, 231);}
            </style>
            @else
            @if (Request::path() == 'login' || Request::path() == 'register' || Request::path() == 'password/reset' || Request::path() == 'email/verify')
            <style> 
            #mainNav{display:none;}
            #mainNav#logo{visibility: hidden;}
            footer{
                display: none;}
            body{background: rgb(19, 161, 231);}
            </style>
            @endif
        @endif
        @yield('logoutModal')
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top" id="mainNav">
            <div class="container-fluid p-0">
            <a class="navbar-brand js-scroll-trigger" href="{{route('principal')}}"><img id="logo" height="160" width="160" src="{{asset('images/logo.png')}}" title="Logo de Ferretería San Felipe" alt="Logo de Ferretería San Felipe"/></a>
                <button class="navbar-toggler navbar-toggler-right btn btn-ligth" type="button" data-toggle="collapse" 
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" 
                aria-label="Toggle navigation">Menú
                <i class="fas fa-bars fa-lg ml-1"></i></button>
                <div class="collapse navbar-collapse flex-column" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto text-white">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route('principal')}}">Ir a inicio</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route('nosotros')}}">Nosotros</a></li>
                    @if (Auth::check())
                        @if(Auth::User()->isAdmin() === 1)
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Bienvenido, Administrador<span class="ml-2 d-lg-inline fas fa-user fa-lg"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{route('home')}}">
                                    <i class="fas fa-home fa-sm fa-fw mr-2 text-gray-400"></i>
                                   Panel de administración
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Cerrar sesión
                                </a>
                                </div>
                            </li>
                        @else
                            <li class="nav-item">
                            <a class="nav-link" href="{{route('carro')}}">
                                <i class="fa fa-shopping-cart fa-lg">
                                    <span class="badge badge-white text-dark btn-circle">{{Cart::getContent()->count()}}</span>
                                </i>
                            </a>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle text-break" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Bienvenido, {{Auth::User()->getFullName()}}<span class="ml-2 d-lg-inline fas fa-user fa-lg"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{route('compras')}}">
                                    <i class="fas fa-shopping-cart fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Mis compras
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Cerrar sesión
                                </a>
                                </div>
                            </li>
                        @endif
                    @else
                            <li class="nav-item no-arrow">
                                <a aria-label="Opciones" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user fa-lg"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{route('login')}}">
                                    <i class="fa fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Iniciar sesión
                                </a>
                                <a class="dropdown-item" href="{{route('register')}}">
                                    <i class="fa fa-user-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Registrarme
                                </a>
                                </div>
                            </li> 
                    @endif
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                          <a class="nav-link d-md-block d-lg-none disabled">Información rápida <span class="sr-only">(current)</span></a>
                        </li>
                            @if (isset($socials) && $socials ?? '')
                             @if($socials->count() <= 0)
                                @if(isset($n_socials) && $n_socials ?? '')
                                    @foreach ($n_socials as $social_test)
                                        <li class="nav-item">
                                        <a class="nav-link" @if ($social_test->link != null)
                                            href="{{$social_test->link}}" aria-label="{{$social_test->name_profile}}"
                                        target="_blank" rel="noopener" @endif><i class="{{$social_test->icon}} fa-lg mr-2"></i>{{$social_test->name_profile}}</a>
                                        </li>
                                    @endforeach
                                @endif
                             @else
                                @foreach ($socials as $social)
                                    <li class="nav-item">
                                    <a class="nav-link" @if ($social->link != null)
                                        href="{{$social->link}}" aria-label="{{$social->name_profile}}"
                                    target="_blank" rel="noopener" @endif><i class="{{$social->icon->icon}} fa-lg mr-2"></i>{{$social->name_profile}}</a>
                                    </li>
                                @endforeach
                            @endif
                        @endif
                      </ul>
                </div>
            </div>
        </nav>
    @yield('content')
         <!--Footer-->
        <footer id="foot">
            <div class="footer-middle">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 text-left">
                            <div class="footer-pad">
                                <h4>Contactános</h4>
                                <ul class="list-unstyled">
                                        @if (isset($informations) && $informations ?? '')
                                          @if($informations->count() <= 0)
                                            @if(isset($n_informations) && $informations ?? '')
                                                @foreach ($n_informations as $info_test)
                                                            <li>
                                                                <a href="{{route('nosotros')}}#ubication"><i class="mr-2 fa fa-map-marker-alt fa-lg"></i>{{$info_test->ubication}}</a>
                                                            </li>
                                                            <li>
                                                                <a><i class="mr-2 fa fa-envelope fa-lg"></i>{{$info_test->email}}</a>
                                                            </li>
                                                            <li>
                                                                <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info_test->normal}} - Horario Normal</a>
                                                            </li>
                                                            <li>
                                                                <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info_test->sunday}} - Horario Domingo</a>
                                                            </li>
                                                            <li>
                                                                <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info_test->holidays}} - Horario Festivos</a>
                                                            </li>
                                                        @endforeach
                                                @endif
                                          @else
                                            @foreach ($informations as $info)
                                                <li>
                                                    <a href="{{route('nosotros')}}#ubication"><i class="mr-2 fa fa-map-marker-alt fa-lg"></i>{{$info->ubication}}</a>
                                                </li>
                                                <li>
                                                    <a><i class="mr-2 fa fa-envelope fa-lg"></i>ferreteriasanfelipe@gmail.com</a>
                                                </li>
                                                <li>
                                                    <a><i class="mr-2 fas fa-phone-volume fa-lg"></i>+56998871979</a>
                                                </li>
                                                <li>
                                                    <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info->normal_working_hours}} - Horario Normal</a>
                                                </li>
                                                <li>
                                                    <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info->working_hours_sunday}} - Horario Domingo</a>
                                                </li>
                                                <li>
                                                    <a><i class="mr-2 fas fa-clock fa-lg"></i>{{$info->holidays_working_hours}} - Horario Festivos</a>
                                                </li>
                                            @endforeach
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="text-center">Redes Sociales</h3>
                            <div class="text-center">
                                @if (isset($socials) && $socials ?? '')
                                  @if($socials->count() <= 0)
                                    @if(isset($n_socials) && $n_socials ?? '')
                                        @foreach ($n_socials as $social_test)
                                            <a title="{{$social_test->name_profile}}" @if ($social_test->link != null)
                                                    href="{{$social_test->link}}" aria-label="{{$social_test->name_profile}}"
                                                target="_blank" rel="noopener"> @endif
                                                    <i class="{{$social_test->icon}} fa-3x social mr-2"></i></a>
                                            @endforeach
                                    @endif
                                  @else
                                    @foreach ($socials as $social)
                                    <a title="{{$social->name_profile}}" @if ($social->link != null)
                                        href="{{$social->link}}" aria-label="{{$social->name_profile}}"
                                     target="_blank" rel="noopener"> @endif
                                        <i class="{{$social->icon->icon}} fa-3x social mr-2"></i></a>
                                    @endforeach
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 copy my-1">
                            <p class="text-center">&copy; Copyright   {{date('Y')}} - Ferrtería San Felipe</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Fin Footer-->
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
        <!-- Core theme JS-->
        <script defer src="{{asset('js/scripts.min.js')}}"></script>
        @yield('scripts')
    </body>
</html>
