@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
             <form id="form" class="form-group" method="POST" action="/home/productos" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de producto</h3>
                              </div>
                          </div>
                          <div class="card-body p-0 my-3">
                              <div class="form-group col-12 col-lg-10 mx-auto">
                                  <div class="input-group mb-2 my-3 mx-auto">
                                      <div class="input-group-prepend">
                                          <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                      </div>
                                    <input aria-label="Nombre" id="validationName" type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Ingrese el nombre" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                                  </div>
                                  <div class="input-group mb-2 my-3 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary">  <i class="fas fa-pen-alt text-white"></i> </div>
                                    </div>
                                <textarea aria-label="Descripción" id="validationDescription" name="description" class="form-control" placeholder="Ingrese la descripción" required>{{old('description')}}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="input-group mb-2 mx-auto">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-dollar-sign text-white"></i></div>
                                                </div>
                                            <input aria-label="Precio" id="validationPrice"type="number"  onkeydown="javascript: return event.keyCode === 8 ||
                                                event.keyCode === 46 ? true : !isNaN(Number(event.key))" name="price" class="form-control" value="{{old('price')}}" placeholder="Ingrese el precio" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="input-group mb-2 mx-auto">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-plus text-white"></i></div>
                                                </div>
                                            <input aria-label="Stock" id="validationStock"  onkeydown="javascript: return event.keyCode === 8 ||
                                                event.keyCode === 46 ? true : !isNaN(Number(event.key))" type="number" name="stock" class="form-control" value="{{old('stock')}}" placeholder="Ingrese el stock" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group mb-2 my-2 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                        </div>
                                        <select aria-label="Categoría" id="validationCategorie" class="custom-select" name="subcategorie_id" required>
                                          <option disabled selected value="">-- Escoga una subcategoría --</option>
                                            @if (isset($subcategories) && $subcategories ?? '')
                                                @foreach ($subcategories as $sc)
                                                    <option value="{{$sc->id}}" {{ old('subcategorie_id') == $sc->id ? 'selected' : '' }}> {{$sc->categorie->name}} - {{$sc->name}}</option>
                                                    
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="input-group mb-2 my-1 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tags text-white"></i> </div>
                                    </div>
                                    <select aria-label="Temporada" class="custom-select" name="season_id">
                                      <option disabled selected>-- Escoga una temporada --</option>
                                      @if (isset($seasons) && $seasons ?? '')
                                        @foreach ($seasons as $s)
                                            <option value="{{$s->id}}" {{old('season_id') == $s->id ? 'selected' : ''}}>{{$s->name}}</option>
                                        @endforeach   
                                      @endif
                                    </select>
                                </div>
                                    <small class="text-primary">La temporada puede quedar <b>vacía</b></small>
                                 <div class="input-group mb-2 my-1 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"> <i class="fa fa-star text-white"></i> </div>
                                    </div>
                                    <select aria-label="Destacado" id="validationDestacated" class="custom-select" name="destacated" required>
                                      <option disabled selected value="">-- ¿Es un producto destacado? --</option>
                                          <option value="si" {{old('destacated') == "si" ? 'selected' : ''}}>Sí</option>
                                          <option value="no" {{old('destacated') == "no" ? 'selected' : ''}}>No</option>
                                    </select>
                                </div>
                                <label class="d-block text-dark" for="Imagen">Imagen</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="custom-file uploadfrm">
                                            <input type="file" accept="image/*" name="image" class="custom-file-input" onchange="readURL(this);" required>
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-md-6 text-center">
                                        <div class="preview">
                                            <img id="img-preview" src="http://placehold.it/150x150" alt="Vista previa" height="150" width="150" class="img-thumbnail border-0">
                                            <small class="d-block">Vista previa de su imagen</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx-auto my-4">
                                    <a href="{{route('productos')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                  </div>  
                              </div>         
                          </div>
                      </div>
                  </form>
              </div>
        </div>  
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection

