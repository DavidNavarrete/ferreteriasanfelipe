@extends('layouts.plantillaHome')

@section('content')
      <div class="card shadow-lg mb-4">
        <div class="card-header bg-primary py-3">
          <h5 class="m-0 font-weight-bold text-white">Descripción de producto</h5>
        </div>
        <div class="card-body text-dark">
          <div class="container-fluid">
          <div class="text-center">
          <div class="card border-0" >
            <div class="row">
              <div class="col-md-4">
                @if (isset($product) && $product ?? '')
                      @foreach ($product as $p)
                        <img height="220" width="220" class="img-fluid mx-auto d-block my-5" src="{{$p->image}}" alt="{{'Imagen de '.$p->name}}" title="{{'Imagen de '.$p->name}}">
                        </div>
                          <div class="col-auto col-lg-8 col-xl-8 my-2">
                            <h2 class="card-title text-center font-weight-bold text-uppercase">{{$p->name}}</h2>
                            <div class="row">
                              <div class="col-lg-6 col-xl-6 mx-auto text-left">
                              <h4 class="card-subtitle font-weight-bold">{{$p->description}}</h4>
                              <h3 class="card-text font-weight-bold text-danger">$ {{$p->price}}</h3>
                              <h5 class="card-text font-weight-bold">Stock: {{$p->stock}}</h5>
                              </div>
                              <div class="col-lg-6 col-xl-6 mx-auto text-left">
                                @if ($p->destacated == 'si')
                                <h5 class="badge badge-primary text-wrap py-3 text-uppercase mr-2">Producto destacado <i class="fa fa-star text-white"></i> </h5>
                                @else
                                <h5 class="badge badge-danger text-wrap py-3 text-uppercase">Producto no destacado </h5>
                                @endif
                              <h5 class="badge badge-warning text-wrap py-3 text-uppercase">{{$p->subcategorie->categorie->name}} - {{$p->subcategorie->name}} </h5>
                              @if ($p->season->name ?? '')
                              <h5 class="badge badge-info text-wrap py-3 text-uppercase">Temporada de {{$p->season->name}}</h5>
                              @else
                              <h5 class="badge badge-danger text-wrap py-3 text-uppercase">Sin temporada</h5>
                              @endif
                              </div>
                            </div>
                          </div>
                           <div class="col-12 text-center">
                              <div class="card-subtitle font-weight-bold">Modificado el: {{$p->updated_at}}</div>
                              <div class="row">
                                <div class="col-6 text-left">
                                  <a href="{{route('productos')}}" class="text-left btn btn-light mx-auto my-1 card-link"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                               </div>
                                 <div class="col-6 text-right">
                                   <a href="{{route('productos.edit', $p->slug)}}" class="btn btn-info text-center my-2 text-white">Editar <span class="fa fa-edit"></span></a>
                                   <a data-toggle="modal" data-target="#openModal{{$p->id}}" role="button" type="button" class="btn btn-danger text-center text-white">Eliminar <span class="fa fa-trash"></span></a>
                                 </div>

                              </div>
                           </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
              <div class="modal fade" id="openModal{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-primary text-white">
                      <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Eliminar producto</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-white active" aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body font-weight-bold">
                        <div class="text-center">
                          <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                        </div>
                        ¿Está seguro de eliminar este producto: "{{$p->name}}" ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                        <a href="{{route('producto.borrar',$p->slug)}}" type="button" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span> </a>
                    </div>
                  </div>
                </div>
              </div>
                @endforeach
      @endif
@endsection