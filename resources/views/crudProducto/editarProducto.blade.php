@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
  <div class="col-12 col-lg-12 pb-5">
    @if (isset($product) && $product ?? '')
    <form id="form" class="form-group" method="POST" action="/home/productos/{{$product->slug}}" enctype="multipart/form-data">
        @method('PUT')                
             @csrf
                  <div class="card shadow-lg">
                    <div class="card-header bg-primary p-0">
                        <div class="text-white text-center py-2">
                            <h3 class="font-weight-bold"> Actualización de producto</h3>
                        </div>
                    </div>
                    <div class="card-body p-0 my-3">
                        <div class="form-group col-12 col-lg-10 mx-auto">
                        <label class="d-block text-dark my-1 font-weight-bold" for="Nombre">Nombre</label>
                            <div class="input-group mb-2 mx-auto">
                                <div class="input-group-prepend">
                                    <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                </div>
                                <input aria-label="Nombre" id="validationName" value="{{$product->name}}" type="text" name="name" class="form-control" placeholder="Ingrese el nombre" required pattern="[A-Za-z0-9 ]+" title="Solo se admiten letras y números.">
                            </div>
                            <label class="d-block text-dark my-1 font-weight-bold" for="Descripcion">Descripción</label>
                            <div class="input-group mb-2 mx-auto">
                              <div class="input-group-prepend">
                                  <div class="input-group-text bg-gradient-primary">  <i class="fas fa-pen-alt text-white"></i> </div>
                              </div>
                            <textarea aria-label="Descripción" id="validationDescription" name="description" class="form-control" placeholder="Ingrese la descripción" required>{{$product->description}}</textarea>
                              </div>
                              <div class="row">
                                <div class="col-12 col-md-6">
                                    <label class="d-block text-dark my-1 font-weight-bold" for="Precio">Precio</label>
                                    <div class="input-group mb-2 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i class="fa fa-dollar-sign text-white"></i></div>
                                        </div>
                                      <input aria-label="Precio" id="validationPrice" value="{{$product->price}}" type="number" onkeydown="javascript: return event.keyCode === 8 ||
                                              event.keyCode === 46 ? true : !isNaN(Number(event.key))" name="price" class="form-control" placeholder="Ingrese el precio" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                  <label class="d-block text-dark my-1 font-weight-bold" for="Stock">Stock</label>
                                  <div class="input-group mb-2 mx-auto">
                                      <div class="input-group-prepend">
                                          <div class="input-group-text bg-gradient-primary"><i class="fa fa-plus text-white"></i></div>
                                      </div>
                                    <input aria-label="Stock" id="validationStock" value="{{$product->stock}}" type="number" onkeydown="javascript: return event.keyCode === 8 ||
                                            event.keyCode === 46 ? true : !isNaN(Number(event.key))" name="stock" class="form-control" placeholder="Ingrese el stock" required>
                                  </div>
                                </div>
                              </div>
                              <label class="d-block text-dark my-1 font-weight-bold" for="Subcategoria">Seleccione una subcategoría</label>
                              <div class="input-group mb-2 mx-auto">
                                  <div class="input-group-prepend">
                                      <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                  </div>
                                  <select aria-label="Categoría" id="validationCategorie" class="custom-select" name="subcategorie_id" required>
                                    <option disabled selected value="">-- Escoga una subcategoría --</option>
                                      @if (isset($subcategories) && $subcategories ?? '')
                                        @foreach ($subcategories as $sc)
                                          <option  value="{{$sc->id}}" @if(isset($product->subcategorie_id) && $product->subcategorie_id == $sc->id) selected @endif>
                                          {{$sc->categorie->name}} - {{$sc->name}}
                                          </option>
                                          @endforeach
                                      @endif
                                  </select>
                              </div>
                              <label class="d-block text-dark my-1 font-weight-bold" for="Temporada">Seleccione una temporada</label>
                              <div class="input-group mb-2 mx-auto">
                              <div class="input-group-prepend">
                                  <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tags text-white"></i> </div>
                              </div>
                              <select aria-label="Temporada" class="custom-select" name="season_id">
                              <option disabled selected>-- Escoga una temporada --</option>
                                @if ($product->season_id ?? '')
                                  @foreach ($seasons as $s)
                                      <option  value="{{$s->id}}" @if(isset($product->season_id) && $product->season_id == $s->id) selected @endif>
                                          {{$s->name}}
                                      </option>
                                    @endforeach
                                  @else
                                  <option disabled selected>-- Seleccione una temporada --</option>
                                    @if (isset($seasons) && $seasons ?? '')
                                      @foreach ($seasons as $s)
                                        <option  value="{{$s->id}}" @if(isset($product->season_id) && $product->season_id == $s->id) selected @endif>
                                            {{$s->name}}
                                        </option>
                                      @endforeach 
                                    @endif
                                  @endif
                              </select>
                          </div>
                          <small class="text-primary">La temporada puede quedar <b>vacía</b></small>
                          <label class="d-block text-dark my-1 font-weight-bold" for="Destacado">¿Es un producto destacado?</label>
                          <div class="input-group mb-2 mx-auto">
                              <div class="input-group-prepend">
                                  <div class="input-group-text bg-gradient-primary"> <i class="fa fa-star text-white"></i> </div>
                              </div>
                              <select aria-label="Destacado" id="validationDestacated" class="custom-select" name="destacated" required>
                                <option disabled selected value="">-- ¿Es un producto destacado? --</option>
                                @if($product->destacated == "si")
                                  <option value="si" selected>Sí</option>
                                  <option value="no">No</option>
                                @else
                                  <option value="si">Sí</option>
                                  <option value="no" selected>No</option>
                                @endif
                              </select>
                          </div>
                          <label class="d-block text-dark" for="Imagen">Imagen</label>
                          <div class="row">
                                    <div class="col-12 col-lg-6 col-xl-4">
                                    <div class="custom-file uploadfrm">
                                            <input type="file" accept="image/*" name="image" class="custom-file-input" onchange="readURL(this);">
                                            <label class="custom-file-label" data-browse="Imagen">Suba su imagen</label>
                                        </div>
                                    <small class="text-primary d-block my-1">La imagen no debe superar los <b>2 MB</b></small>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-6 text-center col-xl-4">
                                      <div class="preview">
                                          <img id="img-actual" src="{{$product->image}}" alt="Vista previa de imagen actual" height="150" width="150" class="img-thumbnail border-0">
                                          <small class="d-block">Imagen actual del producto</small>
                                      </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-12 text-center col-xl-4">
                                      <div class="preview">
                                          <img id="img-preview" src="https://placehold.it/150x150" alt="Vista previa de imagen nueva" height="150" width="150" class="img-thumbnail border-0">
                                          <small class="d-block">Nueva imagen del producto</small>
                                      </div>
                                    </div>
                                </div>
                          <div class="mx-auto my-4">
                              <a href="{{route('productos')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                              <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                            </div>  
                        </div>         
                    </div>
                  </div>
                  </form>
                @endif
              </div>
        </div>
@endsection
@section('scripts')
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection
