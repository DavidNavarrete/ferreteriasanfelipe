@extends('layouts.plantillaHome')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header bg-primary py-3">
          <h5 class="m-0 font-weight-bold text-white">Listado de productos</h5>
        </div>
        <div class="card-body">
          <div class="container-fluid">
            <!-- Barra de busqueda -->
          <form class="col-sm-8 col-md-8 col-lg-6 d-none d-sm-inline-block p-0">
            <div class="input-group">
              <input type="text" name="searchproduct" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
              <div class="input-group-append">
              <button aria-label="Buscador" class="btn btn-primary" type="submit">
                  <i class="fas fa-search fa-sm"></i>
              </button>
              </div>
            </div>
          </form>
          <ul class="navbar-nav ml-auto">
            <!-- Boton para abrir la barra de busqueda -->
            <li class="nav-item dropdown no-arrow d-sm-none text-right">
              <a class="nav-link dropdown-toggle" aria-label="Buscador" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw fa-2x text-primary"></i>
              </a>
              <!-- Fin Boton para abrir la barra de busqueda -->
              <!-- Barra de busqueda en pantallas pequeñas -->
              <div class="dropdown-menu dropdown-menu p-3 shadow" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" name="searchproduct" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
                    <div class="input-group-append">
                      <button aria-label="Buscador" class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
                <!-- Fin Barra de busqueda en pantallas pequeñas -->
            </li>
          </ul>
           <!-- Resultados de búsqueda-->
            <div class="my-2">
              @if(isset($query) && $query ?? '') 
                <div class="alert alert-success">
                  <h6 class="d-inline">
                    @if (isset($products) && $products->count() > 0)
                      Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                    @else
                      No existen resultados para: <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> 
                    @endif
                  </h6>
                </div>
                <div class="d-flex justify-content-start">
                  <a href="{{route('productos')}}" type="submit" role="button" class="btn btn-outline-primary">Mostrar todos</a>
                </div>
              @endif
          </div>
              @include('common.success')
               <!-- Fin Resultados de búsqueda-->
              <div class="text-right my-1 p-0">
                <a href="{{route('productos.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
              </div>
              <div class="row">
                @if (isset($products) && $products ?? '')
                  @foreach ($products as $product)
                  <div class="col-auto col-sm-6 col-md-6 col-lg-4 col-xl-3 mx-auto text-dark my-1">
                    <div class="card shadow h-100">
                    <!-- src=/images/productos/{{$product->image}} -->
                    <img class="card-img-top" src="{{$product->image}}" width="240" height="231" title="{{'Imagend de '.$product->name}}" alt="{{'Imagend de '.$product->name}}">
                      <div class="card-body">
                      <h5 title="{{$product->name}}" class="card-title font-weight-bold text-uppercase">{{Str::limit($product->name, $limit="12", $end="...")}}</h5>
                      <a href="{{route('productos.show', $product->slug)}}" class="btn btn-outline-primary font-weight-bold">Ver más</a>     
                      <div class="text-left">
                        <a aria-label="Editar" title="Editar" href="{{route('productos.edit', $product->slug)}}" class="btn btn-info text-white text-center my-2 mr-2"> <span class="fa fa-edit"></span></a>
                        <a aria-label="Eliminar" title="Eliminar" data-toggle="modal" data-target="#openModal{{$product->id}}" role="button" type="button" class="btn btn-danger text-white"> <span class="fa fa-trash text-white"></span></a>
                        <div class="modal fade" id="openModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header bg-primary text-white">
                                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Eliminar producto</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span class="text-white active" aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body font-weight-bold">
                                  <div class="text-center">
                                    <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                                  </div>
                                  ¿Está seguro de eliminar este producto: "{{$product->name}}" ?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                  <a href="{{route('producto.borrar', $product->slug)}}" type="button" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span> </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                  @endforeach  
                @endif
              </div>
              <div class="d-flex justify-content-center my-4">
                @if (isset($products) && $products ?? '')
                  {{$products->links()}}        
                @endif
              </div>
            </div>
          </div>
        </div>
@endsection