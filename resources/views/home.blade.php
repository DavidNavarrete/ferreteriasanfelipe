@extends('layouts.plantillaHome')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center col-lg-4">
        @if(isset($earningsToday) && $earningsToday ?? '')
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Ganancias (día)<i class="fas fa-piggy-bank fa-lg"></i></div>
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">$ {{$earningsToday}}</h5>
                    <p class="card-text">Estas son las ganancias generadas en el día</p>
                </div>
            </div>
            @else
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Sin ganancias (día) <i class="fas fa-piggy-bank"></i></div>
                <div class="card-body">
                    <h5 class="card-title">$0</h5>
                    <p class="card-text">Aún no se han generado ganancias este día.</p>
                </div>
            </div>
            @endif
        </div>
        <div class="col-12 text-center col-lg-4">
        @if(isset($earningsMonth) && $earningsMonth ?? '')
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Ganancias (mes) <i class="fas fa-piggy-bank fa-lg"></i></div>
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">$ {{$earningsMonth}}</h5>
                    <p class="card-text">Estas son las ganancias generadas en el mes de @if(isset($month) && $month ?? '') {{$month->formatLocalized('%B')}} @endif</p>
                </div>
            </div>
            @else
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Sin ganancias (mes) <i class="fas fa-piggy-bank"></i></div>
                <div class="card-body">
                    <h5 class="card-title">$0</h5>
                    <p class="card-text">Aún no se han generado ganancias este mes.</p>
                </div>
            </div>
            @endif
        </div>
        <div class="col-12 text-center col-lg-4">
        @if(isset($earningsYear) && $earningsYear ?? '')
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Ganancias (año) <i class="fas fa-piggy-bank fa-lg"></i></div>
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">$ {{$earningsYear}}</h5>
                    <p class="card-text">Estas son las ganancias generadas en el año</p>
                </div>
            </div>
            @else
            <div class="card text-white bg-success mb-3 shadow h-100" style="max-width: 18rem;">
                <div class="card-header bg-white text-dark font-weight-bold text-center text-uppercase">Sin ganancias (año)<i class="fas fa-piggy-bank"></i></div>
                <div class="card-body">
                    <h5 class="card-title">$0</h5>
                    <p class="card-text">Aún no se han generado ganancias.</p>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
