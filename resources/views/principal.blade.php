@extends('layouts.plantilla')
@section('style')
<link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"></noscript>
@endsection
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')
<br>
<!--Navbar Categorías-->
<div class="col-lg-12">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary text-white" style="border-radius: 5px;">
        <a class="navbar-brand d-md-block d-lg-none">Categorías</a>
        <button class="navbar-toggler active border-0" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fa-lg text-white"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav text-left">
                     @if (isset($categories) && $categories ?? '')
                        @if($categories->count() <= 0)
                        <!-- Si no existen categorias, se agregan categorias automaticas para no dejarlas en blanco -->
                        @if(isset($n_categories) && $n_categories ?? '')
                                @foreach ($n_categories as $categorietest)    
                                    <li class="nav-item dropdown no-arrow">
                                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$categorietest->categorie}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item font-weight-bold" href="/?search={{$categorietest->categorie}}">Ver todos</a>
                                        <div class="dropdown-divider"></div>
                                                    <a aria-label="Submenu" class="dropdown-item font-italic" href="/?search={{$categorietest->subcategorie}}">{{$categorietest->subcategorie}}</a>
                                        </div>
                                    @endforeach
                                @endif
                                    </li>
                        @else
                        @foreach ($categories as $categorie)    
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$categorie->name}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item font-weight-bold" href="/?search={{$categorie->name}}">Ver todos</a>
                            <div class="dropdown-divider"></div>
                            @if ($categorie->subcategories ?? '' && $categorie->subcategories->count() < 0)
                            @foreach ($categorie->subcategories as $subcategorie)
                                        <a aria-label="Submenu" class="dropdown-item font-italic" href="/?search={{$subcategorie->name}}">{{$subcategorie->name}}</a>
                                    @endforeach
                            @endif
                            </div>
                        </li>
                        @endforeach
                    @endif
                @endif
          </ul>
        </div>
      </nav>
</div>
<!--Fin Navbar Categorías-->
<!--Buscador-->
<div class="col-lg-12 my-2">
    <form class="col-lg-6 p-0" action="" method="GET">
        <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="¿Qué producto buscas?" aria-label="Buscador">
            <div class="input-group-append">
            <button aria-label="Buscador" class="btn btn-primary" type="submit">
                <i class="fa fa-search"></i>
            </button>
            </div>
        </div>
    </form>
</div>
<!--Fin Buscador-->
<!-- Productos destacados-->
    <section class="destacated-products" id="destacated-products">
        <div class="container-fluid">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Productos Destacados</h2>
            <h3 class="section-subheading text-muted">Estos son nuestros productos destacados</h3>
        </div>
        <!--Modal Descripción Productos-->
        @include('modalProductoDetalle')
        <!--Fin Modal Descripción Productos-->
        <div class="container-fluid content-row my-1">
            <section id="pd">
                <div class="owl-carousel owl-theme">
                    @if (isset($products_destacated) && $products_destacated ?? '')   
                     <!-- Si no existen productos destacados, se agregan productos automaticos para no dejarlas en blanco --> 
                        @if($products_destacated->count() <= 0)
                        @if(isset($n_products) && $n_products ?? '')
                            @foreach ($n_products as $product_test)
                                        <div class="card item h-100 w-100"> 
                                        <a aria-label="Abrir más información" class="btn-open-modal disabled" role="button">
                                                <i class="fa fa-eye"></i>
                                            </a>     
                                        <img class="card-img-top" loading="lazy" height="275" width="230" src='{{$product_test->image}}' alt="{{'Imagen de '.$product_test->name}}" title="{{'Imagen de '.$product_test->name}}">
                                                <div class="card-body">
                                                    <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($product_test->name, $limit="17", $end="...")}}</div>
                                                    <div class="card-text text-left font-weight-bold price">${{$product_test->price}}</div>
                                                    <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($product_test->updated_at, $limit="10", $end="")}}</p></div>
                                                    @if ($product_test->stock > 0)
                                                        <a href="{{route('carro.agregar', $product_test->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                                    @else
                                                        <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                                    @endif
                                                </div>
                                            </div> 
                                    @endforeach
                            @endif
                        @else
                        @foreach ($products_destacated as $product_destacated)
                            <div class="card item h-100 w-100"> 
                            <a aria-label="Abrir más información" class="btn-open-modal" role="button" onclick="showModalProduct('{{$product_destacated}}')">
                                    <i class="fa fa-eye"></i>
                                </a>     
                                <!-- src=/images/productos/{{$product_destacated->image}} -->
                            <img class="card-img-top border-0" loading="lazy" height="275" width="230" src="{{$product_destacated->image}}" alt="{{'Imagen de '.$product_destacated->name}}" title="{{'Imagen de '.$product_destacated->name}}">
                                    <div class="card-body">
                                        <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($product_destacated->name, $limit="17", $end="...")}}</div>
                                        <div class="card-text text-left font-weight-bold price">${{$product_destacated->price}}</div>
                                        <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($product_destacated->updated_at, $limit="10", $end="")}}</p></div>
                                        @if ($product_destacated->stock > 0)
                                            <a href="{{route('carro.agregar', $product_destacated->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                        @else
                                            <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                        @endif
                                    </div>
                                </div> 
                        @endforeach
                        @endif
                        @endif
                </div>
            </section>
        </div>
    </div>
 </section>
<!-- Productos destacados-->
<section>
    <div class="container-fluid">
    <div class="text-center">
        <h2 class="section-heading text-uppercase">Productos de temporada</h2>
        <h3 class="section-subheading text-muted">Estos son nuestros productos de temporada</h3>
    </div>
    <!--Modal Descripción Productos-->
    @include('modalProductoDetalle')
    <!--Fin Modal Descripción Productos-->
    <div class="container-fluid content-row my-1">
        <section id="ps">
            <div class="owl-carousel owl-theme">
                @if (isset($products_season) && $products_season ?? '')   
                 <!-- Si no existen productos de temporada, se agregan productos automaticos para no dejarlas en blanco --> 
                 @if($products_season->count() <= 0) 
                 @if(isset($n_seasons) && $n_seasons ?? '')
                @foreach ($n_seasons as $season_test)
                        <div class="card item h-100">
                        <img class="card-img-top" loading="lazy" height="275" width="230" src="{{$season_test->season_image}}" alt="{{'Imagen de '.$season_test->name}}" title="{{'Imagen de '.$season_test->name}}">
                            <div class="card-body">
                            <div class="card-title font-weight-bold text-uppercase">Temporada de {{Str::limit($season_test->name, $limit="20", $end="...")}}</div>
                            <div class="more">
                                <a href="/?search={{$season_test->name}}" class="text-primary font-weight-bold">Ver más</a>
                                </div>  
                            </div>
                        </div>
                            <div class="card item h-100 w-100">      
                                <a aria-label="Abrir más información" class="btn-open-modal" role="button">
                                    <i class="fa fa-eye"></i>
                                </a>          
                            <img class="card-img-top" loading="lazy" height="275" width="230" src="{{$season_test->image}}" alt="{{'Imagen de '.$season_test->name_product}}" title="{{'Imagen de '.$season_test->name_product}}">
                                    <div class="card-body">
                                        <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($season_test->name_product, $limit="17", $end="...")}}</div>
                                        <div class="card-text text-uppercase font-weight-bold badge badge-primary text-wrap py-1">{{$season_test->name}}</div>
                                        <div class="card-text text-left font-weight-bold price">${{$season_test->price}}</div>
                                        <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($season_test->updated_at, $limit="10", $end="")}}</p></div>
                                        @if ($season_test->stock > 0)
                                                <a href="{{route('carro.agregar', $season_test->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                            @else
                                                <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                            @endif
                                    </div>
                                </div> 
                            @endforeach
                            @endif
                 @else
                    @foreach ($products_season as $ps)
                    <div class="card item h-100">
                    <!-- src=/images/temporadas/{{$ps->season->image}} -->
                    <img class="card-img-top" loading="lazy" height="275" width="230" src="{{$ps->season->image}}" alt="{{'Imagen de '.$ps->season->name}}" title="{{'Imagen de '.$ps->season->name}}">
                        <div class="card-body">
                        <div class="card-title font-weight-bold text-uppercase">Temporada de {{Str::limit($ps->season->name, $limit="20", $end="...")}}</div>
                        <div class="more">
                            <a href="/?search={{$ps->season->name}}" class="text-primary font-weight-bold">Ver más</a>
                            </div>  
                        </div>
                    </div>
                        <div class="card item h-100 w-100">      
                            <a aria-label="Abrir más información" class="btn-open-modal" role="button" onclick="showModalProduct('{{$ps}}')">
                                <i class="fa fa-eye"></i>
                            </a>          
                            <!-- src=/images/productos/{{$ps->image}} -->
                        <img class="card-img-top" loading="lazy" height="275" width="230" src="{{$ps->image}}" alt="{{'Imagen de '.$ps->name}}" title="{{'Imagen de '.$ps->name}}">
                                <div class="card-body">
                                    <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($ps->name, $limit="17", $end="...")}}</div>
                                    <div class="card-text text-uppercase font-weight-bold badge badge-primary text-wrap py-1">{{$ps->season->name}}</div>
                                    <div class="card-text text-left font-weight-bold price">${{$ps->price}}</div>
                                    <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($ps->updated_at, $limit="10", $end="")}}</p></div>
                                    @if ($ps->stock > 0)
                                            <a href="{{route('carro.agregar', $ps->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                        @else
                                            <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                        @endif
                                </div>
                            </div> 
                        @endforeach
                        @endif
                    @endif
            </div>
        </section>
    </div>
</div>
</section>
<!--Fin Productos Destacados-->
<!--Categorías-->
    <section>
        <div class="container-fluid">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Categorías</h2>
                <h3 class="section-subheading text-muted">Seleccione una categoría para ver los productos relacionados</h3>
            </div>
        <!--Categorias-->
                <div class="categorie-box">
                    <div class="container-fluid content-row">
                         <div class="row">
                             @if (isset($categories) && $categories ?? '')
                              <!-- Si no existen categorias, se agregan categorias automaticas para no dejarlas en blanco --> 
                                @if($categories->count() <= 0)
                                  @if(isset($n_categories) && $n_categories ?? '')
                                    @foreach ($n_categories as $categorie_test)     
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 text-center">
                                                            <div class="categorie border h-100 shadow">
                                                                <div class="circle shadow d-inline-block">
                                                                    <i class="fa fa-tag fa-3x my-4 text-primary" aria-hidden="true"></i>
                                                                </div>
                                                                <div class="categorie-title my-2">
                                                                    <h5>{{$categorie_test->categorie}}</h5>
                                                                </div>
                                                                <div class="more my-2">
                                                                <a href="/?search={{$categorie_test->categorie}}" class="text-primary font-weight-bold">Ver más</a>
                                                                </div>      
                                                            </div>
                                                        </div>              
                                                @endforeach
                                        @endif
                             @else
                                    @foreach ($categories as $categorie)     
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 text-center">
                                                <div class="categorie border h-100 shadow">
                                                    <div class="circle shadow d-inline-block">
                                                        <i class="fa fa-tag fa-3x my-4 text-primary" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="categorie-title my-2">
                                                        <h5>{{$categorie->name}}</h5>
                                                    </div>
                                                    <div class="more my-2">
                                                    <a href="/?search={{$categorie->name}}" class="text-primary font-weight-bold">Ver más</a>
                                                    </div>      
                                                </div>
                                            </div>              
                                    @endforeach
                                  @endif
                                 @endif
                            </div>	
                    </div>
                </div>
            </div>
</section>
<!--Fin Categorías-->

<div class="container-fluid my-4">
    <div class="text-center">
        <h2 class="section-heading text-uppercase">Productos</h2>
        <h3 class="section-subheading text-muted">Estos son nuestros productos</h3>
    </div>
    <div class="row justify-content-center">
        @if (isset($products) && $products ?? '')
          <!-- Si no existen productos, se agregan productos automaticos para no dejarlas en blanco --> 
         @if($products->count() <= 0)
         @if(isset($n_products) && $n_products ?? '')
            @foreach ($n_products as $product_test)   
                <div class="col-auto col-sm-6 mx-0 col-md-4 col-lg-3 col-xl-3 text-dark my-2">
                    <div class="card shadow h-100 w-100">      
                        <a aria-label="Abrir más información" class="btn-open-modal" role="button">
                            <i class="fa fa-eye"></i>
                        </a>           
                            <img class="card-img-top" loading="lazy" height="275" width="230" src="{{$product_test->image}}" alt="{{'Imagen de '.$product_test->name}}" title="{{'Imagen de '.$product_test->name}}">
                                <div class="card-body">
                                    <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($product_test->name, $limit="22", $end="...")}}</div>
                                    <div class="card-text text-left font-weight-bold price">${{$product_test->price}}</div>
                                    <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($product_test->updated_at, $limit="10", $end="")}}</p></div>
                                    @if ($product_test->stock > 0)
                                        <a href="{{route('carro.agregar', $product_test->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                    @else
                                        <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                    @endif
                                </div>
                            </div> 
                        </div>
                @endforeach
            @endif
         @else
                @foreach ($products as $product)   
                <div class="col-auto col-sm-6 mx-0 col-md-4 col-lg-3 col-xl-3 text-dark my-2">
                    <div class="card shadow h-100 w-100">      
                        <a aria-label="Abrir más información" class="btn-open-modal" role="button" onclick="showModalProduct('{{$product}}')">
                            <i class="fa fa-eye"></i>
                        </a>           
                        <!-- src=/images/productos/{{$product->image}} -->
                            <img class="card-img-top border-0" loading="lazy" height="275" width="230" src="{{$product->image}}" alt="{{'Imagen de '.$product->name}}" title="{{'Imagen de '.$product->name}}">
                                <div class="card-body">
                                    <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($product->name, $limit="22", $end="...")}}</div>
                                    <div class="card-text text-left font-weight-bold price">${{$product->price}}</div>
                                    <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($product->updated_at, $limit="10", $end="")}}</p></div>
                                    @if ($product->stock > 0)
                                        <a href="{{route('carro.agregar', $product->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                    @else
                                        <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                    @endif
                                </div>
                            </div> 
                        </div>
                @endforeach
              @endif
            @endif
    </div>
    <div class="d-flex justify-content-center my-4">
        @if (isset($products) && $products ?? '')  
            {{$products->links()}}
        @endif
      </div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="{{asset('js/carouselproducts.min.js')}}"></script>
<script defer src="{{asset('js/openModal.min.js')}}"></script>
{!! Toastr::message() !!}
@endsection

