@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
      <div class="col-12 col-lg-12 pb-5">
          @if (isset($categorie) && $categorie ?? '')
          <form id="form" class="form-group" method="POST" action="/home/categorias/{{$categorie->slug}}" enctype="multipart/form-data">
                  @method('PUT')
                      @csrf
                            <div class="card shadow-lg my-2">
                                <div class="card-header bg-primary p-0">
                                    <div class="text-white text-center py-2">
                                        <h3 class="font-weight-bold"> Actualización de categorías</h3>
                                    </div>
                                </div>
                                <div class="card-body p-0 my-4">
                                    <div class="form-group col-12 col-lg-10 mx-auto">
                                    <label class="text-dark font-weight-bold" for="Nombre">Nombre de categoría</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i class="fa fa-tags text-white"></i></div>
                                            </div>
                                            <input aria-label="Nombre" id="validationName" type="text" name="name" class="form-control" value="{{$categorie->name}}" placeholder="Ingrese el nombre de la categoría" required>
                                        </div>
                                        <div class="mx-auto my-4">
                                            <a href="{{route('categorias')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                            <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif
                  </div>
              </div>
@endsection

