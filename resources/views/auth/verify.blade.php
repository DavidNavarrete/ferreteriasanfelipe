@extends('layouts.plantilla')

@section('content')
<div class="container my-4">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-6 mx-auto">
        <div class="card card-signin my-5 border text-dark shadow-lg">
          <div class="card-body">
            <h5 class="card-title text-center font-weight-bold">Verificación de correo electrónico</h5>
            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                        Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico
                    </div>
                    @endif
                    Antes de continuar, consulte su correo electrónico para ver si hay un enlace de verificación. Si no recibió el correo electrónico,
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">Click aquí para solicitar otro</button>.
                    </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
