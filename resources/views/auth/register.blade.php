@extends('layouts.plantilla')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5 shadow-lg">
          <div class="card-body">
            <h5 class="card-title text-center font-weight-bold">Registrarme</h5>
            <form class="form-signin" method="POST" action="{{ route('register') }}">
                @csrf
              <div class="form-label-group">
                <input placeholder="Ingrese su nombre" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label class="text-dark" for="name">Nombre</label>
              </div>
              <div class="form-label-group">
                <input placeholder="Ingrese su apellido" id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                @error('lastname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label class="text-dark" for="lastname">Apellido</label>
              </div>
              <div class="form-label-group">
                <input placeholder="Ingrese su correo electrónico" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                           </span>
                      @enderror
                <label class="text-dark" for="email">Correo Electrónico</label>
              </div>
              <div class="form-label-group">
                <input placeholder="Ingrese su contraseña" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label class="text-dark" for="password">Contraseña</label>
              </div>
              <div class="form-label-group">
                <input placeholder="Confirme su contraseña" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                <label class=" text-dark" for="password-confirm">Confirmar contraseña</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Registrarme</button>
              <hr class="my-3">
              <div class="text-center">
                <p class="text-dark ">¿Ya tienes una cuenta? <a role="button" href="{{route('login')}}" class="btn btn-link text-uppercase text-primary" type="submit">Iniciar sesión</a></p>
                  <a href="{{route('principal')}}" class="btn btn-outline-primary my-1 mx-auto"> <i class="fa fa-arrow-left mr-2"></i>Ir a inicio</a>   
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
