@extends('layouts.plantilla')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5 border">
          <div class="card-body">
            <h5 class="card-title text-center font-weight-bold">Cambio de contraseña</h5>
            <form class="form-signin" method="POST" action="{{ route('password.update') }}">
                @csrf
                 <input type="hidden" name="token" value="{{ $token }}">
              <div class="form-label-group">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                 @error('email')
                       <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                        </span>
                   @enderror
                   <label for="email">Correo electrónico</label>
              </div>
              <div class="form-label-group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label for="password">Contraseña</label>
              </div>
              <div class="form-label-group">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label for="password">Confirmar contraseña</label>
              </div>
              <div class="col-lg-8 mx-auto">
                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase my-1">Reestablecer</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection