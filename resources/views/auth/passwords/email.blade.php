@extends('layouts.plantilla')

@section('content')
<div class="container my-4">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-6 mx-auto">
        <div class="card card-signin my-5 border text-dark shadow-lg">
          <div class="card-body">
            <h5 class="card-title text-center font-weight-bold">Reestablecer contraseña</h5>
            @if (session('status'))
                        <div class="alert alert-success " role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
            <form class="form-signin" method="POST" action="{{ route('password.email') }}">
                @csrf
              <div class="form-label-group">
                <input placeholder="Ingrese su correo electrónico" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                 @error('email')
                       <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                        </span>
                   @enderror
                   <label for="email">Correo electrónico</label>
              </div>
              <div class="col-lg-6 text-center mx-auto">
                <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase my-1">Enviar enlace</button>
              </div>
              <div class="row">
                <div class="mx-auto col-8 col-sm-6 text-center">
                  <a href="{{route('principal')}}" class="btn btn-outline-primary my-1 text-center"> <i class="fa fa-arrow-left mr-2 d-none d-sm-inline"></i>Ir a inicio</a>   
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
