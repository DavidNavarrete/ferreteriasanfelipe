@extends('layouts.plantilla')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5 shadow-lg">
          <div class="card-body">
            <h5 class="card-title text-center font-weight-bold">Iniciar sesión</h5>
            <form class="form-signin" method="POST" action="{{ route('login') }}">
                @csrf
              <div class="form-label-group">
               
                <input placeholder="Ingrese su correo electrónico" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus>
                 @error('email')
                       <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                        </span>
                   @enderror
                   <label class="text-dark" for="email">Correo electrónico</label>
              </div>
              <div class="form-label-group">
                <input placeholder="Ingrese su contraseña" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <label class="text-dark" for="password">Contraseña</label>
              </div>
              <div class="custom-control custom-checkbox mb-3">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="text-dark" class="form-check-label" for="remember">
                        Recordar contraseña
                    </label> 
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase my-1" type="submit">Iniciar sesión</button>
                @if (Route::has('password.request'))
                <div class="text-center">
                  <a role="button" href="{{route('password.request')}}" class="btn btn-link text-uppercase text-center" type="submit"><i class="fa fa-lock-open mr-2"></i>¿Olvidaste tu contraseña?</a>
                </div>
                @endif
              <hr class="my-3">
              <div class="text-center">
                <p class="text-dark">¿Aún no tienes una cuenta? <a role="button" href="{{route('register')}}" class="btn btn-link text-uppercase text-primary" type="submit">Regístrate</a></p>
                  <a href="{{route('principal')}}" class="btn btn-outline-primary my-1 mx-auto"> <i class="fa fa-arrow-left mr-2"></i>Ir a inicio</a>   
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
