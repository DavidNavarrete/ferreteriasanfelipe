@extends('layouts.plantillaHome')

@section('content')
@include('common.errors')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
            <form id="form" class="form-group" method="POST" action="/home/redes" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg p-0">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de redes sociales</h3>
                              </div>
                          </div>
                          <div class="card-body p-0">
                            <div class="form-group col-12 col-lg-11 mx-auto">
                            <label class="d-block d-sm-none text-dark font-weight-bold my-1"for="RedSocial">Seleccione la red social</label>
                                <div class="input-group mb-2 my-2 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                    </div>
                                    <select aria-label="Icono" id="validationIcon" class="custom-select" name="icon_id" required>
                                      <option disabled selected value="">-- Escoga una red social --</option>
                                      @if (isset($icons) && $icons ?? '')
                                        @foreach ($icons as $i)
                                    <option  value="{{$i->id}}" {{old('icon_id') == $i->id ? 'selected': ''}}>{{$i->name}}</option>
                                        @endforeach
                                        </select>  
                                      @endif
                                </div>
                                <label class="d-block d-sm-none text-dark font-weight-bold" for="Enlace">Link o enlace</label>
                                <div class="input-group mb-2 my-1 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-link text-white"></i></div>
                                    </div>
                                <input aria-label="Enlace" type="url" id="validationLink" name="link" class="form-control" value="{{old('link')}}" placeholder="Ingrese el link o enlace (Incluya https)">
                            </div>
                            <small class="text-primary">El campo <b>'Enlace'</b> puede quedar vacío.</small>
                            <label class="d-block d-sm-none text-dark font-weight-bold" for="NombrePerfil">Nombre del perfil o número</label>
                                <div class="input-group mb-2 my-1 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-user text-white"></i></div>
                                    </div>
                                <input aria-label="Nombre de perfil" type="text" id="validationNameProfile" name="name_profile" class="form-control" value="{{old('name_profile')}}" placeholder="Ingrese el nombre del perfil o número teléfonico" required>
                                </div>
                                <div class="mx-auto my-4">
                                    <a href="{{route('redes')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                </div>
                            </div>    
                          </div>
                      </div>
                  </form>
              </div>
@endsection

