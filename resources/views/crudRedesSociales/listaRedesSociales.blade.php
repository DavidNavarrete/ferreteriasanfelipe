@extends('layouts.plantillaHome')

@section('content')
          <div class="card shadow-lg mb-4">
            <div class="card-header bg-primary py-3">
              <h4 class="m-0 font-weight-bold text-white">Panel de administración de redes sociales</h4>
            </div>
            <div class="card-body">
              <div class="container-fluid">
            <form class="col-sm-8 col-md-8 col-lg-6 d-none d-sm-inline-block p-0">
              <div class="input-group">
                <input type="text" name="searchsocialnetwork" class="form-control small border text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
                <div class="input-group-append">
                  <button aria-label="Buscador" class="btn btn-primary" type="submit">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="navbar-nav ml-auto">
              <!-- Boton para abrir la barra de busqueda -->
              <li class="nav-item dropdown no-arrow d-sm-none text-right">
                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-search fa-fw fa-2x text-primary"></i>
                </a>
                <!-- Fin Boton para abrir la barra de busqueda -->
                <!-- Barra de busqueda en pantallas pequeñas -->
                <div class="dropdown-menu dropdown-menu p-3 shadow" aria-labelledby="searchDropdown">
                  <form class="form-inline mr-auto w-100 navbar-search">
                    <div class="input-group">
                      <input type="text" name="searchsocialnetwork" class="form-control border small text-dark" placeholder="¿Qué buscas?" aria-label="Buscador">
                      <div class="input-group-append">
                        <button aria-label="Buscador" type="submit" class="btn btn-primary">
                          <i class="fas fa-search fa-sm"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                  <!-- Fin Barra de busqueda en pantallas pequeñas -->
              </li>
            </ul>
              <!-- Resultados de búsqueda-->
              <div class="my-2">
                @if(isset($query) && $query ?? '') 
                  <div class="alert alert-success">
                    <h6 class="d-inline">
                      @if (isset($socialnetworks) && $socialnetworks->count() > 0)
                        Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                      @else
                        No existen resultados para: <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> 
                      @endif
                    </h6>
                  </div>
                  <div class="d-flex justify-content-start">
                    <a href="{{route('redes')}}" type="submit" role="button" class="btn btn-outline-primary">Mostrar todas</a>
                  </div>
                @endif
                
             </div>
              <!-- Fin Resultados de búsqueda-->
              @include('common.success')
              <div class="table-responsive">
                <table class="table text-dark font-weight-bold" id="dataTable" width="100%" cellspacing="0">
                    <div class="text-right my-1 p-0">
                        <a href="{{route('redes.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
                    </div>
                  <thead class="bg-gradient-primary text-white">
                    <tr>
                      <th>Nombre</th>
                      <th>Enlace</th>
                      <th>Perfil o número</th>
                      <th>Ícono</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tfoot class="bg-gradient-primary text-white">
                    <tr>
                      <th>Nombre</th>
                      <th>Enlace</th>
                      <th>Perfil o número</th>
                      <th>Ícono</th>
                      <th>Opciones</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @if (isset($socialnetworks) && $socialnetworks ?? '')
                      @foreach ($socialnetworks as $socialnetwork)
                        <tr>
                        <td>{{$socialnetwork->icon->name}}</td>
                        <td><a target="_blank" rel="noopener" href="{{$socialnetwork->link}}">Ir al enlace de {{$socialnetwork->icon->name}}</a></td>
                        <td>{{$socialnetwork->name_profile}}</td>
                        <td><i class="{{$socialnetwork->icon->icon}} fa-2x text-center text-primary"></i></td>
                        <td>
                        <a aria-label="Editar" href="{{route('redes.edit', $socialnetwork->slug)}}" role="button" type="button" class="btn btn-info text-white my-1"><span class="fa fa-edit"></span></a>
                        <a aria-label="Eliminar" data-toggle="modal" data-target="#openModal{{$socialnetwork->slug}}" role="button" type="button" class="btn btn-danger"> <span class="fa fa-trash text-white"></span></a>
                      <!-- Modal para eliminar-->
                        <div class="modal fade" id="openModal{{$socialnetwork->slug}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header bg-primary">
                              <h5 class="modal-title text-white" id="exampleModalLabel">Eliminar red social</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="text-white" aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body font-weight-bold text-center">
                              <div>
                                <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                              </div>
                              ¿Está seguro de eliminar esta red social: "{{$socialnetwork->icon->name}}" ?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                              <a href="{{route('red.borrar',$socialnetwork->slug)}}" type="button" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span> </a>
                          </div>
                          </div>
                        </div>
                      </div>
                        </td>
                        </tr>
                        @endforeach
                    @endif
                  </tbody>
                </table>
                <div class="d-flex justify-content-center mx-auto">
                  @if (isset($socialnetworks) && $socialnetworks ?? '')
                    {{$socialnetworks->links()}}  
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>      
@endsection


