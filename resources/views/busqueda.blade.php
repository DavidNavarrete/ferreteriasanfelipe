@extends('layouts.plantilla')
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')
<br>
<!--Navbar Categorías-->
<div class="col-lg-12">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary text-white" style="border-radius: 5px;">
        <a class="navbar-brand d-md-block d-lg-none">Categorías</a>
        <button class="navbar-toggler active border-0" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fa-lg text-white"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav text-left">
          @if (isset($categories) && $categories ?? '')
                        @if($categories->count() <= 0)
                        <!-- Si no existen categorias, se agregan categorias automaticas para no dejarlas en blanco -->
                        @if(isset($n_categories) && $n_categories ?? '')
                                @foreach ($n_categories as $categorietest)    
                                    <li class="nav-item dropdown no-arrow">
                                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$categorietest->categorie}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item font-weight-bold" href="/?search={{$categorietest->categorie}}">Ver todos</a>
                                        <div class="dropdown-divider"></div>
                                                    <a aria-label="Submenu" class="dropdown-item font-italic" href="/?search={{$categorietest->subcategorie}}">{{$categorietest->subcategorie}}</a>
                                        </div>
                                    @endforeach
                                @endif
                                    </li>
                        @else
                        @foreach ($categories as $categorie)    
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$categorie->name}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item font-weight-bold" href="/?search={{$categorie->name}}">Ver todos</a>
                            <div class="dropdown-divider"></div>
                            @if ($categorie->subcategories ?? '' && $categorie->subcategories->count() < 0)
                            @foreach ($categorie->subcategories as $subcategorie)
                                        <a aria-label="Submenu" class="dropdown-item font-italic" href="/?search={{$subcategorie->name}}">{{$subcategorie->name}}</a>
                                    @endforeach
                            @endif
                            </div>
                        </li>
                        @endforeach
                    @endif
                @endif
          </ul>
        </div>
      </nav>
</div>
<!--Fin Navbar Categorías-->
<!--Buscador-->
<div class="col-lg-12 my-2">
<form class="col-lg-6 p-0" action="" method="GET">
    <div class="input-group">
        <input aria-label="Buscador" type="text" name="search" class="form-control" placeholder="¿Qué producto buscas?">
        <div class="input-group-append">
        <button aria-label="Buscador" class="btn btn-primary" type="submit">
            <i class="fa fa-search"></i>
        </button>
        </div>
    </div>
</form>
</div>
<!--Fin Buscador-->
<div>
</div>
    <div class="container-fluid my-4">
        <div class="text-left">
            @if (isset($query) && $query ?? '')
                @if (isset($resultsproduct) && $resultsproduct->count() != 0)
                    <h3 class="text-uppercase">Los resultados para la búsqueda de "{{$query}}" son: </h3>        
                    <p class="text-right justify-content-end font-italic"><b>{{$resultsproduct->count()}}</b> resultado(s)</p>
                @else
                    <h3 class="text-uppercase">No existen resultados para su búsqueda</h3>
                @endif  
                <div class="d-flex justify-content-start p-2">
                    <a href="{{route('principal')}}" role="button" class="btn btn-outline-primary">Mostrar todos</a>
                  </div>
            @endif
        </div>
        @include('modalProductoDetalle')
        <div class="container-fluid">
            <div class="row justify-content-center">
                @if (isset($resultsproduct) && $resultsproduct ?? '')
                    @foreach ($resultsproduct as $result)   
                        <div class="col-auto col-sm-6 col-md-5 col-lg-3 col-xl-3 text-dark my-2">
                            <div class="card shadow my-2 p-0 h-100">      
                                <a class="btn-open-modal" role="button" onclick="showModalProduct('{{$result}}')">
                                    <i class="fa fa-eye"></i>
                                </a>    
                                <!-- src=/images/productos/{{$result->image}} -->
                                <img  class="card-img-top" height="231" width="231" src="{{$result->image}}" alt="{{'Imagen de ' . $result->name}}" title="{{'Imagen de ' . $result->name}}">
                                        <div class="card-body">
                                            <div class="card-tittle font-weight-bold text-uppercase">{{Str::limit($result->name, $limit="17", $end="...")}}</div>
                                            <div class="card-text text-left font-weight-bold price">${{$result->price}}</div>
                                            <div class="modified"><b>Modificado:</b><p class="font-italic">{{Str::limit($result->updated_at, $limit="10", $end="")}}</p></div>
                                            @if ($result->stock > 0)
                                                        <a href="{{route('carro.agregar', $result->slug)}}" class="btn btn-primary w-100 text-uppercase font-weight-bold">Agregar al carro</a>
                                                    @else
                                                        <a class="btn btn-primary disabled text-white w-100 text-uppercase font-weight-bold">Sin stock</a>
                                            @endif
                                        </div>
                                    </div> 
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="d-flex justify-content-center my-4">
            @if (isset($resultsproduct) && $resultsproduct ?? '')
                {{$resultsproduct->links()}}
            @endif
          </div>
    </div>
@endsection
@section('scripts')
<script defer src="{{asset('js/openModal.min.js')}}"></script>
@endsection