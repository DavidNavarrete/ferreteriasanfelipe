@extends('layouts.plantilla')
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')

<div class="container-fluid p-1 my-2">
        <div class="card-header bg-primary text-white">
                <div class="row">
                    <div class="col-6">
                        <h6 class="font-weight-bold d-inline text-uppercase">Mi carro de compras</h6>  
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{route('principal')}}" class="btn btn-outline-light font-weight-bold">Seguir viendo</a>
                    </div>
                </div>
        </div>
</div>

<div class="container-fluid p-1 my-3">
@if(isset($cartItems) && $cartItems ?? '')
@if ($cartItems->count() > 0)
  <div class="row m-1">
      <div class="col-6 p-0">
        <a target="_blank" href="{{route('verPDF')}}" type="button" class="btn btn-primary my-1">Guardar cotización en PDF <i class="fas fa-file-pdf fa-lg ml-2 d-none d-sm-inline-block"></i></a>
      </div>
      <div class="col-6 d-flex justify-content-end p-0">
              <form action="{{route('carro.limpiar')}}" method="get">
                  <button type="submit" class="btn btn-outline-info">Limpiar carro <i class="fa fa-trash ml-2"></i></button>
                </form>
      </div>
  </div>
@endif
<h6 class="text-center font-weight-bold my-1">Esta cotización tiene una validez de 4 días hábiles, una vez creada.</h6>
<h6 class="text-center font-weight-bold my-1">Los productos cotizados se retiran directamente en la tienda presentando la cotización correspondiente, no se entregan a domicilio.</h6>
<div id="no-more-tables">
            <table class="col-md-12 table-hover p-2">
            <caption><i>Lista de productos</i></caption>
        		<thead class="bg-primary">
        			<tr class="text-white">
        				<th class="numeric">N°</th>
        				<th class="numeric">ID</th>
        				<th class="numeric">Producto</th>
        				<th class="numeric">Nombre</th>
        				<th class="numeric">Precio</th>
        				<th class="numeric">Cantidad</th>
        				<th class="numeric">Opciones</th>
        			</tr>
        		</thead>
        		<tbody>
            @foreach($cartItems as $item)
            <tr class="my-1">
              <td data-title="N°">{{$loop->iteration}}</td>
              <td data-title="ID">{{$item->id}}</td>
              <td data-title="Producto"> 
                <!-- "/images/productos/" -->
                <img class="rounded my-3 p-auto img-fluid" src="{{$item->associatedModel->image}}" alt="prewiew" width="80" height="80">
              </td>
              <td data-title="Nombre" class="font-weight-bold text-uppercase">{{$item->name}}</td>
              <td data-title="Precio" class="price font-weight-bold">
              {{$item->getPriceSum()}}
              </td>
              <td data-title="Cantidad"> 
              <form action="{{route('carro.actualizar', $item->id)}}">
                <input class="col col-md-6" name="quantity" type="number" value="{{$item->quantity}}">
                <button class="btn btn-primary my-1" type="submit"> <i class="fas fa-sync-alt"></i> </button>
              </form>
              </td>
              <td data-title="Opciones">
              <a href="{{route('carro.eliminar', $item->id)}}" aria-label="Eliminar" type="button" class="btn btn-outline-danger btn-xs ml-2 my-1">
                <i class="fa fa-trash" aria-hidden="true"></i>
              </a>
              </td>
            </tr>
            @endforeach
            @endif
        		</tbody>
        	</table>
        </div>
    </div>
    <div class="card-footer bg-white text-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-7 text-sm-left text-md-right">
                                <h3 class="font-weight-bold text-uppercase">Total a pagar</h3>
                                <div class="row">
                                    <div class="col-12 text-sm-left col-md-12 text-md-right mx-auto">
                                        <h2><span class="badge badge-primary"><i class="fa fa-dollar-sign"></i>
                                        @if (isset($total) && $total ?? '')
                                            {{$total}}
                                        @endif
                                        </span></h2>
                                    </div>
                                </div>
                            </div>
                        <div class="col-5 p-0 text-right mt-5">
                          <form method="POST">
                          @if (isset($total) && $total ?? '')      
                              <a href="{{route('checkout', [$total, Auth::User()->id])}}" type="button" class="btn btn-outline-primary">Continuar <i class="fa fa-arrow-right fa-lg ml-2 d-none d-sm-inline-block"></i></a>
                              <small class="d-block text-danger my-1">Simulación de pago</small>
                          @endif
                            </form>
                        </div>
                        </div>
                    </div>
    </div>
</div>
@endsection
@section('scripts')
{!! Toastr::message() !!}
@endsection