<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel">¿Estás seguro(a)?</h5>
          <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
            <span class="text-white" aria-hidden="false">x</span>
          </button>
        </div>
        <div class="modal-body text-dark">Seleccione "Sí, cerrar sesión" para cerrar la sesión</div>
        <div class="modal-footer">
          <button class="btn btn-light" type="button" data-dismiss="modal">Cancelar</button>
       <a class="btn btn-primary" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Sí, cerrar sesión') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
        </form>
        </div>
      </div>
    </div>
  </div>