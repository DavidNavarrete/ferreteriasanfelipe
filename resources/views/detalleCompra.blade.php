@extends('layouts.plantilla')
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')
    
<div class="container-fluid p-1 my-2">
        <div class="card-header bg-primary text-white">
                <div class="row">
                    <div class="col-6 text-center">
                        <h6 class="font-weight-bold d-inline text-uppercase">Detalle de mis compras</h6>  
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{route('compras')}}" class="btn btn-outline-light font-weight-bold">Volver a mis compras</a>
                    </div>
                </div>
        </div>
</div>
@if(isset($detailBuys) && $detailBuys ?? '')
<div class="container-fluid">
  <div id="no-more-tables">
            <table class="col-md-12 table-hover p-2">
            <caption><i>Detalles de la compra</i></caption>
        		<thead class="bg-primary">
        			<tr class="text-white">
        				<th class="numeric">N°</th>
                        <th class="numeric">N° Orden</th>
        				<th class="numeric">Producto</th>
        				<th class="numeric">Nombre</th>
        				<th class="numeric">Precio</th>
        				<th class="numeric">Cantidad</th>
                        <th class="numeric">Fecha de compra</th>
        			</tr>
        		</thead>
        		<tbody>
            @foreach($detailBuys as $buy)
            <tr class="my-1">
              <td data-title="N°">{{$loop->iteration}}</td>
              <td data-title="Orden" class="font-weight-bold">{{$buy->order}} <i class="fas fa-star text-primary"></i> </td>
              <td data-title="Producto"> 
                <!-- "/images/productos/" -->
                <img class="rounded my-3 p-auto img-fluid" src="{{$buy->image}}" alt="prewiew" width="80" height="80">
              </td>
              <td data-title="Nombre" class="font-weight-bold text-uppercase">{{$buy->name}}</td>
              <td data-title="Precio" class="price font-weight-bold">{{$buy->price}}</td>
              <td data-title="Cantidad">{{$buy->quantity}}</td>
              <td data-title="Fecha de compra">{{$buy->created_at}}</td>
            </tr>
            @endforeach
        		</tbody>
        	</table>
        </div>
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="card-title font-weight-bold text-uppercase">Total final de la compra</h3>
                <h2><span class="badge badge-primary"><i class="fa fa-dollar-sign"></i>
                                        @if (isset($total) && $total ?? '')
                                            {{$total}}
                                        @endif
                                        </span></h2>
                </div>
            </div>
    </div>
</div>
@endif

@endsection