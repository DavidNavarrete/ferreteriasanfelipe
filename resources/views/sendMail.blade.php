<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cotización</title>
    <style>
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.8cm;
            color: white;
            text-align: center;
            line-height: 26px;
        }
    </style>
</head>
<body>
<header class="bg-white">
  <img style="margin-left: 550px" src="{{asset('images/logo.png') }}" alt="Logo" height="55" width="150">
</header>
<h5 class="font-weight-bold text-uppercase text-center">Mi compra</h5>
<p class="text-center font-italic my-0">Los productos comprados se retiran directamente en la tienda presentando la cotización o documento correspondiente, no se entregan a domicilio.</p>

    <!-- Datos del cliente -->
    <table class="table">
      <thead class="bg-primary">
        <tr class="text-white">
          <th scope="col">Nombre del cliente</th>
          <th scope="col">Correo</th>
          <th scope="col">Fecha</th>
        </tr>
      </thead>
      <tbody>
        @foreach($user as $u)
        <tr>
          <td class="font-italic">{{$u->name.$u->lastname}} -- [{{$u->id}}]</td>
          <td class="font-italic">
          {{$u->email}}
          </td>
          <td>
          <!-- Muestra la fecha actual -->
          {{date('Y-m-d H:i:s') }}
          </td>
        </tr>
          @endforeach
      </tbody>
    </table>
    <!-- Productos del carrito de compras del cliente -->
    <table class="table">
      <thead class="bg-primary">
        <tr class="text-white">
          <th scope="col">N°</th>
          <th scope="col">Orden de compra</th>
          <th scope="col">Producto</th>
          <th scope="col">Nombre</th>
          <th scope="col">Cantidad</th>
        </tr>
      </thead>
      <tbody>
        @foreach($details as $item)
        <tr>
          <th scope="row">{{$loop->iteration}}</th>
          <td class="font-weight-bold">{{$item->border}}</td>
          <td> 
          <img src="{{$item->pimage}}" alt="prewiew" width="80" height="80"></td>
          <td class="font-weight-bold text-uppercase">{{$item->pname}}</td>
          <td>
          {{$item->bquantity}}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <!-- Total a pagar -->
    <h5 class="font-weight-bold text-uppercase text-center">Total a pagar</h5>
    <h2 class="font-weight-bold text-center price">
        $ {{$total}}
    </h2>
    <footer class="bg-primary">
        <p class="text-center">&copy; Copyright   {{date('Y')}} - Ferrtería San Felipe</p>
    </footer>
</body>
</html>