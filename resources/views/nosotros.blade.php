@extends('layouts.plantilla')
@section('logoutModal')
    @include('modalLogout')
@endsection
@section('content')
<!--Sobre nosotros/Descripción-->
@if (isset($informations) && $informations ?? '')
        @foreach ($informations as $info) 
        <div class="container my-4">
        <div class="row">
            <div class="col-12 col-sm-7 col-md-6 col-lg-8 col-xl-8">
                <div class="text-center">
                    <h3 class="text-left text-uppercase">¿Quiénes somos?</h3>
                    <hr class="bar">
                    <p class="text-left font-italic">
                        <i class="fa fa-arrow-circle-right fa-1x text-primary"></i>
                        @if($info->description ?? '')
                            {{$info->description}} 
                        @else
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus ipsum quia, veritatis nobis atque iusto inventore hic voluptates odit rerum architecto soluta fugiat velit, optio incidunt error dolores, et beatae?
                        @endif
                    </p>
                    <img src="{{asset('/images/portada.jpg')}}" alt="Imagen de Ferretería San Felipe" title="Imagen de Ferretería San Felipe" class="img-fluid rounded shadow">
                </div>
            </div>
            <div class="col-12 col-sm-5 col-md-6 col-lg-4 col-xl-4">
                <div class="card text-center shadow-lg my-5 ml-4">
                    <div class="card-header bg-primary text-white text-left font-weight-bold">
                        Información
                    </div>
                    <div class="card-body text-left">
                    @if (isset($socials) && $socials ?? '')
                             @if($socials->count() <= 0)
                                @if(isset($n_socials) && $n_socials ?? '')
                                    @foreach ($n_socials as $social_test)
                                    <i class="{{$social_test->icon}} fa-lg text-primary mr-2"></i>
                                <a @if ($social_test->link != null)
                                    href="{{$social_test->link}}" target="_blank" rel="noopener"
                                   @endif  title="Ir a {{$social_test->icon}}" class="card-title text-dark"><b>{{$social_test->name_profile}}</b></a><br>
                                    @endforeach
                                @endif
                             @else
                                @foreach ($socials as $social)
                                <i class="{{$social->icon->icon}} fa-lg text-primary mr-2"></i>
                                <a @if ($social->link != null)
                                    href="{{$social->link}}" target="_blank" rel="noopener"
                                   @endif  title="Ir a {{$social->icon->name}}" class="card-title text-dark"><b>{{$social->name_profile}}</b></a><br>
                                @endforeach
                            @endif
                        @endif
                        <div class="my-4">
                            <h5>Horario</h5>
                            <ul class="list-unstyled">
                            @if (isset($informations) && $informations ?? '')
                                          @if($informations->count() <= 0)
                                            @if(isset($n_informations) && $informations ?? '')
                                                @foreach ($n_informations as $info_test)
                                                <li>
                                                    <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info_test->normal}} - Horario Normal</a>
                                                </li>
                                                <li>
                                                    <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info_test->sunday}} - Horario Domingo</a>
                                                </li>
                                                <li>
                                                    <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info_test->holidays}} - Horario Festivos</a>
                                                </li>
                                                        @endforeach
                                                @endif
                                          @else
                                            @foreach ($informations as $info)
                                            <li>
                                                <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info->normal_working_hours}} - Horario Normal</a>
                                            </li>
                                            <li>
                                                <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info->working_hours_sunday}} - Horario Domingo</a>
                                            </li>
                                            <li>
                                                <a class="text-dark"><i class="fas fa-clock fa-lg text-primary mr-2"></i>{{$info->holidays_working_hours}} - Horario Festivos</a>
                                            </li>
                                            @endforeach
                                        @endif
                                    @endif
                            </ul>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
        </div>
        <!--Sobre nosotros/Descripción-->
        <!--Valores-->
        <div class="container my-4">
        <div class="text-left">
            <h3 class="text-left text-uppercase">Nuestra misión</h3>
            <hr class="bar">
        </div>
            <p class="text-left font-italic">
                <i class="fa fa-heart text-danger fa-1x"></i>
                @if($info->mission ?? '')
                    {{$info->mission}} 
                @else
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus ipsum quia, veritatis nobis atque iusto inventore hic voluptates odit rerum architecto soluta fugiat velit, optio incidunt error dolores, et beatae?
                @endif
            </p>
        </div>
        <div class="container my-4">
        <div class="text-left">
            <h3 class="text-left text-uppercase">Nuestra visión</h3>
            <hr class="bar">
        </div>
            <p class="text-left font-italic">
                <i class="fa fa-heart text-danger fa-1x"></i>
                @if($info->vision ?? '')
                    {{$info->vision}} 
                @else
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus ipsum quia, veritatis nobis atque iusto inventore hic voluptates odit rerum architecto soluta fugiat velit, optio incidunt error dolores, et beatae?
                @endif
            </p>
        </div>
        <!--Valores-->
        <!--Mapa-->
        <div class="container my-4">
            <h3 class="section-heading text-left text-uppercase">Nuestra ubicación</h3>
            <hr class="bar">
            <div class="embed-responsive embed-responsive-16by9 rounded shadow-lg" id="ubication">
                <iframe loading="lazy" @if(isset($ubicationMap) && $ubicationMap ?? '') src="{{$ubicationMap}}" title="Ubicación Ferretería San Felipe - {{$ubicationMap}}"  @endif allowfullscreen></iframe>
            </div>
        </div>
        <!--Mapa-->
        @endforeach
    @endif
@endsection