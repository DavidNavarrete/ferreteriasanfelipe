<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PrincipalController@index')->name('principal');
Route::get('/nosotros', 'UsController@index')->name('nosotros');
Route::get('/agregar-al-carro/{product}', 'CartController@add')->name('carro.agregar');
Route::get('/carro-de-compras', 'CartController@index')->name('carro')->middleware('verified');
Route::get('/carro-de-compras/eliminar/{itemId}', 'CartController@destroy')->name('carro.eliminar')->middleware('verified');
Route::get('/carro-de-compras/actualizar/{itemId}', 'CartController@update')->name('carro.actualizar')->middleware('verified');
Route::get('/carro-de-compras/limpiarCarro', 'CartController@clear')->name('carro.limpiar')->middleware('verified');
Route::get('/checkout/{total}/user/{id}', 'CheckoutController@initTransaction')->name('checkout');
Route::post('/checkout/webpay/response', 'CheckoutController@response')->name('checkout.webpay.response');
Route::post('/checkout/webpay/finish', 'CheckoutController@finish')->name('checkout.webpay.finish');
Route::get('/pdfcotizacion', 'PDFController@printPDF')->name('verPDF')->middleware('verified');
Route::get('/mis-compras', 'BuyController@index')->name('compras')->middleware('verified');
Route::get('/detalle-compras/{order}', 'BuyController@show')->name('detalle')->middleware('verified');

Auth::routes(['verify' => true]);

Route::group(['middleware'=>['auth', 'admin']], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('home/subcategorias', 'SubcategorieController');
    Route::get('home/subcategorias/destroy/{subcategoria}','SubcategorieController@destroy')->name('subcategoria.borrar');
    Route::get('home/subcategorias', 'SubcategorieController@index')->name('subcategorias');
    Route::resource('home/categorias', 'CategorieController');
    Route::get('home/categorias/destroy/{categoria}','CategorieController@destroy')->name('categoria.borrar');
    Route::get('home/categorias', 'CategorieController@index')->name('categorias');
    Route::resource('home/productos', 'ProductController');
    Route::get('home/productos/destroy/{producto}', 'ProductController@destroy')->name('producto.borrar');
    Route::get('home/productos', 'ProductController@index')->name('productos');
    Route::resource('home/temporadas', 'SeasonController');
    Route::get('home/temporadas/destroy/{temporada}', 'SeasonController@destroy')->name('temporada.borrar');
    Route::get('home/temporadas', 'SeasonController@index')->name('temporadas');
    Route::resource('home/redes', 'SocialController');
    Route::get('home/redes', 'SocialController@index')->name('redes');
    Route::get('home/redes/destroy/{rede}', 'SocialController@destroy')->name('red.borrar');
    Route::resource('home/imagenes', 'ProductImagesController');
    Route::get('home/imagenes', 'ProductImagesController@index')->name('imagenes');
    Route::get('home/imagenes/destroy/{imagene}', 'ProductImagesController@destroy')->name('imagenes.borrar');
    Route::resource('home/informacion', 'InformationController');
    Route::get('home/informacion', 'InformationController@index')->name('informacion');
    Route::resource('home/orden', 'OrderController');
    Route::get('home/orden', 'OrderController@index')->name('orden');
});


