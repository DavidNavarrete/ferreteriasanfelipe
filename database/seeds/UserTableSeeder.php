<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = \App\Role::where('name', 'Admin')->pluck('id')->first();
        $user = new User();
        $user->name = 'Administrador';
        $user->lastname = 'Admin';
        $user->email = 'Administrador@gmail.com';
        $user->password = bcrypt('query');
        $user->role_id = $role_admin;
        $user->save();
    }
}
