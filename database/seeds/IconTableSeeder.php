<?php

use App\Icon;
use Illuminate\Database\Seeder;

class IconTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $icon = new Icon();
        $icon->name = 'Facebook';
        $icon->icon = 'fab fa-facebook-square';
        $icon->save();
        $icon = new Icon();
        $icon->name = 'Whatsapp';
        $icon->icon = 'fab fa-whatsapp';
        $icon->save();
        $icon = new Icon();
        $icon->name = 'Correo electrónico';
        $icon->icon = 'fa fa-envelope';
        $icon->save();
        $icon = new Icon();
        $icon->name = 'Instagram';
        $icon->icon = 'fab fa-instagram';
        $icon->save();
    }
}
